#include "assembly_symbol_table.hpp"

unsigned Stack::get ( const std::string& name ) const
{
    unsigned offset = 0;
    for ( const_iterator i = stack.begin(); i != stack.end(); ++i )
        if ( i->name == name )
            return offset;
        else
            offset += i->size;
    return -1; // will give segfault in resulting code
}
unsigned Stack::get_function_scope ( ) const
{
    unsigned offset = 0;
    for ( const_iterator i = stack.begin(); i != stack.end(); ++i )
        if ( i->dummy && i->name == "func" )
            return offset;
        else
            offset += i->size;
    return -1; // will give segfault in resulting code
}

bool Stack::exists ( const std::string& name ) const
{
    if ( name == "" )
    {
        for ( const_iterator i = stack.begin(); i != stack.end(); ++i )
            if ( i->dummy )
                return true;
    }
    else
    {
        for ( const_iterator i = stack.begin(); i != stack.end(); ++i )
            if ( !i->dummy && i->name == name )
                return true;
    }
    return false;
}

/*#include <iostream>
using std::cout;/// debug*/

void Stack::push ( const label& label )
{
    unsigned size = 0;
    if ( label.type == INTEGER )
        size = sizeof_int;
    else if ( label.type == FLOAT )
        size = sizeof_float;
    else if ( label.type == STRING )
        size = sizeof_string;
    //std::cout << '+' << size << ' ' << label.name << '\n';
    stack.push_front( StackElement(label.name, size) );
}
void Stack::pop()
{
    //std::cout << '-' << stack[0].size << ' ' << stack[0].name << '\n';
    stack.pop_front();
}
void Stack::push_scope ()
{
    //cout << "+0\n";
    stack.push_front( StackElement() );
}
void Stack::push_function_scope()
{
    //cout << '+' << sizeof_string << '\n';
    stack.push_front( StackElement("func",sizeof_string,true) );
}
void Stack::pop_function_scope()
{
    iterator i = stack.begin();
    for ( ; i != stack.end(); ++i )
        if ( i->dummy && i->name=="func" )
            break;
    stack.erase ( stack.begin(), i+1 );
}

void Stack::pop_scope()
{
    //cout << '-';
    //unsigned size = 0; /// debug
    iterator i = stack.begin();
    for ( ; i != stack.end(); ++i )
        if ( i->dummy )
            break;
        /*else
            size += i->size;
    size += i->size;
    cout << size << '\n';*/
    stack.erase ( stack.begin(), i+1 );
}
