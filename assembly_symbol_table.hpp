#ifndef assembly_symbol_table_hpp_
#define assembly_symbol_table_hpp_
#include "symbol_table.hpp"
#include <deque>


struct StackElement
{
    std::string name;
    unsigned size;
    bool dummy;

    StackElement ( const std::string &name, unsigned size, bool dummy = false )
        :name ( name ), size ( size ), dummy ( dummy ) {}

    explicit StackElement ( unsigned size = 0 )
        : name(""), size(size), dummy(true) {}
};
class Stack
{
    protected:
        typedef std::deque<StackElement>::iterator iterator;
        typedef std::deque<StackElement>::const_iterator const_iterator;
        std::deque<StackElement> stack;
        unsigned sizeof_int, sizeof_string, sizeof_float;
    public:
        explicit Stack (
            unsigned sizeof_int = sizeof(int),
            unsigned sizeof_string = sizeof(char*),
            unsigned sizeof_float = sizeof(double)
        ) : sizeof_int ( sizeof_int ), sizeof_string ( sizeof_string ), sizeof_float ( sizeof_float ) {}

        /// \return offset of given symbol
        unsigned get ( const std::string& name ) const;
        unsigned get_function_scope ( ) const;
        bool exists ( const std::string& name ) const;
        void push ( const label& label );
        void push_scope ();
        void push_function_scope();
        void pop_function_scope();
        void pop_scope();
        void pop();
};


#endif
