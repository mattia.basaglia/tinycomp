#include "compiler.hpp"
#include "generator.hpp"
#include <vector>
#include <cstdio>
#include "push_pop_optimizer.hpp"
static std::vector<std::string> files; /// \todo file handler ( passed to parser )

int launch_command(const std::string&cmd)
{
    //std::cout << "$ " << cmd << '\n';
    return system(cmd.c_str());
}

void compile_file ( const std::string & in_name, const std::string &out_name, bool debug )
{

    std::ifstream source ( in_name.c_str() );
    std::ofstream output ( out_name.c_str() );

    std::stringstream interm;
    x86_64_gas_generator gen (interm);


    if ( debug )
        for ( unsigned i = 0; i < files.size(); ++i )
            gen.debug_file ( files[i], i+1 );
    files.push_back ( in_name );

    parser ( source,
            gen,
            error_handler(std::cerr),
            debug,
            in_name,
            files.size()
            ) .parse();

    push_pop_optimizer pp ( gen.push(), gen.pop(), &gen.mov );
    pp.check(interm,output);
}
void assemble_file ( const std::string & in_name, const std::string &out_name, bool debug )
{

    std::string as = "as ";
    if ( debug )
        as += "-g ";
    as += in_name+" -o "+out_name;

    launch_command ( as );

    //system ( as.c_str() );
}


void link_files ( const std::vector<std::string> &in_files,
                 const std::string &out_name,
                 const std::string& link_settings  )
{
    std::string cmd = "gcc -o " + out_name;
    for ( std::vector<std::string>::const_iterator i = in_files.begin();
            i != in_files.end(); ++i )
        cmd += ' '+*i;
    launch_command(cmd+link_settings);
}
