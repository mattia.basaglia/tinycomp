#ifndef COMPILER_HPP_
#define COMPILER_HPP_
#include "parser.hpp"
#include <iostream>
#include <vector>
#include <fstream>


int launch_command(const std::string&cmd);

void compile_file ( const std::string & in_name, const std::string &out_name, bool debug );

void assemble_file ( const std::string & in_name, const std::string &out_name, bool debug );

void link_files ( const std::vector<std::string> &in_files,
                 const std::string &out_name="a.out",
                 const std::string& link_settings=""  );
#endif // COMPILER_HPP_
