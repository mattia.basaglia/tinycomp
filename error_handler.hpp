#ifndef ERROR_HANDLER_HPP_
#define ERROR_HANDLER_HPP_
#include <iostream>


class generic_error_handler
{
    public:
    virtual void error ( const std::string &file,
                        unsigned line,
                        const std::string& what,
                        int severity ) = 0;
    virtual ~generic_error_handler() {}
};

class error_handler : public generic_error_handler
{
    protected:
        std::streambuf *pbuf;
        std::ostream out;
        int min_severity;
    public:
        error_handler ( std::ostream& os, int min_severity = -1 )
            : pbuf ( os.rdbuf() ), out ( pbuf ), min_severity(min_severity) {}
        error_handler ( const error_handler& o )
            : pbuf ( o.pbuf ), out ( pbuf ), min_severity(o.min_severity) {}
        error_handler& operator= ( const error_handler& o )
        {
            pbuf = o.pbuf;
            out.rdbuf ( pbuf );
            min_severity = o.min_severity;
            return *this;
        }

        void error ( const std::string &file,
                    unsigned line,
                    const std::string& what,
                    int severity )
        {
            if ( severity > min_severity )
                out << file << ':' << line << ": " <<
                    ( severity == 0 ? "Warning" : "Error" ) << ": "
                    << what << '\n';
        }
};

#endif // ERROR_HANDLER_HPP_
