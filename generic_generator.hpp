#ifndef GENERIC_GENERATOR_HPP
#define GENERIC_GENERATOR_HPP
#include "symbol_table.hpp"
#include <iostream>
#include <sstream>
class generic_generator
{
    protected:
        std::streambuf* out_buf;
        std::ostream out;
        std::ostringstream func_cache;
        /// \return mangled name for f
        virtual std::string mangle(const function& f) { return f.name; }
    public:
        SymbolTable *pst;

        generic_generator ( std::ostream& outs )
            : out_buf ( outs.rdbuf() ), out ( out_buf ), pst ( 0 ) {}
        generic_generator ( const generic_generator& o )
            : out_buf ( o.out_buf ), out ( out_buf ),
                func_cache(o.func_cache.str()), pst(0) {}
        generic_generator& operator= ( const generic_generator& o )
        {
            out_buf = o.out_buf;
            out.rdbuf ( out_buf );
            func_cache.str ( o.func_cache.str() );
            pst = o.pst;
            return *this;
        }

    public:

        virtual void assign (
                         const label& dest,
                         const label& op1,
                         const label& op2,
                         const std::string &operand ) = 0;
        virtual void assign (
                         const label& dest,
                         const label& val ) = 0;
        virtual void assign (
                         const label& dest,
                         const label& val,
                         const std::string &operand
                         ) = 0;
        virtual void cast ( const label& dest,
                            const label& source ) = 0;
        virtual void begin_parameters() = 0;
        virtual void push_parameter ( const function& func, const label& param, const label&val ) = 0;
        virtual void call ( const label& get_return, const function& func ) = 0;
        virtual void f_return ( const label& value ) = 0;
        virtual void f_return ( ) = 0;
        /**
            \brief Outputs global data
            \note will call functions()
        */
        virtual void data () = 0;
        virtual void text() = 0;
        /// defines program entry point
        virtual void start() = 0;
        virtual void jump ( const label& to ) = 0;
        virtual void jump_if ( const label& expr, const label& to, bool negated ) = 0;
        virtual void exit ( const label& value ) = 0;
        virtual void exit () = 0;
        virtual void place_label(const label& lab) = 0;
        virtual void enter_function ( const function& func ) = 0;
        virtual void leave_function() = 0;
        virtual void functions() = 0;
        virtual void clear_functions() = 0;
        virtual void enter_scope() = 0;
        virtual void leave_scope() = 0;
        virtual void raw( const std::string& text) = 0;
        virtual void debug_file ( const std::string& name, unsigned file_counter ) = 0;
        virtual void debug_line ( unsigned line, unsigned file_counter ) = 0;

        virtual bool willcast ( ValueType to, ValueType from ) = 0;

};

#endif // GENERIC_GENERATOR_HPP
