#include "lexer.hpp"

typedef lexer::token token;

token lexer::scan()
{
    str = "";
    char c;
    do
    {
        in.get(c);
        if ( !in )
            return EOF;
        skip_comment(c);
        if ( c == '\n' )
            line++;
    }
    while (  std::isspace(c) );


    str += c;
    if ( std::isalpha(c) )
        return scan_keyword();
    else if ( std::isdigit(c) )
        return scan_number();
    else if ( c == '"' || c == '\'' )
        return scan_string(c);
    else if ( c == '=' || c == '!' || c == '+' || c == '-' || c == '*'
             || c == '/' || c == '%' || c == '<' || c == '>' )
    {

        if ( in.peek() == '=' )
            str += in.get();
        return token(OPERATOR,str);
    }
    else if ( c == '$' || c == ';' || c == '(' || c == ')' || c == '{'
            || c == '}' || c == ',' || c == '[' || c == ']' )
        return token(OPERATOR,str);
    else
    {
        ok = false;
        return scan(); // restart
    }
}

void lexer::skip_comment( char &c )
{
    if ( c == '/' )
    {
        if ( in.peek() == '/' )
            while ( c != '\n' )
                in.get(c);
        else if ( in.peek() == '*' )
        {
            while ( true )
            {
                in.get(c);
                if ( c == '*' && in.peek() == '/' )
                {
                    in.get();
                    c = ' ';
                    return;
                }
            }
        }
    }
}

token lexer::scan_keyword()
{
    char c;
    while ( true )
    {
        in.get(c);
        if ( !in || !std::isalnum(c) )
        {
            in.putback(c);
            break;
        }
        str += c;
    }
    return token(IDENTIFIER,str);
}

token lexer::scan_number()
{
    token_type t = INTEGER;
    char c;
    while ( true )
    {
        in.get(c);
        if ( c == '.' )
            break;
        else if ( !in || !std::isalnum(c) )
        {
            in.putback(c);
            break;
        }
        str += c;
    }

    if ( c == '.' )
    {
        t = FLOAT;
        while ( true )
        {
            str += c;
            in.get(c);
            if ( !in || !std::isalnum(c) )
            {
                in.putback(c);
                break;
            }
        }
    }

    return token(t,str);
}

token lexer::scan_string(char delim)
{
    char c;
    str = "";
    while ( true )
    {
        in.get(c);
        if ( c == '\n' )
            line++;

        if ( !in || c == delim )
        {
            break;
        }
        else if ( c == '\\' )
        {
            in.get(c); // discard backslash
            if ( c == '\n' )
                line++;

            if ( c == 'n' )
                str += '\n';
            else if ( c == 'r' )
                str += '\r';
            else
                str += c;
        }
        else
            str += c;
    }
    if ( str.size() == 1 )
        return token ( CHAR, str );
    return token ( STRING, str );
}

