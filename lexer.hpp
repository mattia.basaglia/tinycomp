#ifndef LEXER_HPP_
#define LEXER_HPP_

#include <iostream>
#include <string>
#include <cctype>

class lexer
{
    protected:
        std::streambuf *pbuf;
        std::istream in;
        std::string str;
        bool ok;
    public:
        unsigned line;
        enum token_type { INTEGER, FLOAT, CHAR, STRING, IDENTIFIER, OPERATOR, EOF };
        struct token
        {
            token_type type;
            std::string str;
            token( token_type type = EOF ) : type ( type ) {}
            token( token_type type, const std::string& str ) : type ( type ), str ( str ) {}
            bool operator== ( token_type t ) { return type == t; }
            bool operator!= ( token_type t ) { return type != t; }
            bool operator== ( const std::string& s ) { return type != STRING && type != CHAR && str == s; }
            bool operator!= ( const std::string& s ) { return type == STRING || type == CHAR || str != s; }
        };

        lexer( std::istream& is ) : pbuf(is.rdbuf()), in ( pbuf ), ok(true), line(1) {}
        lexer( const lexer& o ) : pbuf(o.pbuf), in ( pbuf ), ok(o.ok), line(o.line) {}
        lexer& operator= ( const lexer& o )
        {
            pbuf = o.pbuf;
            in.rdbuf ( pbuf );
            ok = o.ok;
            line = o.line;
            return *this;
        }

        void rdbuf ( std::streambuf *buf )
        {
            pbuf = buf;
            in.rdbuf ( pbuf );
        }
        std::streambuf *rdbuf()
        {
            return pbuf;
        }
        bool is_ok(){return ok;} /// \todo in parser as well ( => in err handler )
        bool error(){return !ok;}
        void clear_error(){ok=true;}

        token scan();

    protected:

        token scan_keyword();

        token scan_number();

        token scan_string(char delim);

        void skip_comment( char &c );

};

#endif
