#include <iostream>
#include <sstream>
#include "lexer.hpp"
#include "parser.hpp"
#include "generator.hpp"
#include "compiler.hpp"
#include <boost/program_options.hpp>
using namespace std;


void test_parser();
void test_compiler();

void test()
{
    //test_parser();
    //test_compiler();
    compile_file("test.cl","test.s",true);
    assemble_file("test.s","test.o",true);

    compile_file("stdlib_x86_64_gas.cl","stdlib_x86_64_gas.s",true);
    assemble_file("stdlib_x86_64_gas.s","stdlib_x86_64_gas.o",true);

    std::vector<std::string> files;
    files.push_back("stdlib_x86_64_gas.o");
    files.push_back("test.o");
    link_files(files);
    launch_command("./a.out");
}

int main ( int argc, char** argv )
{
    using namespace boost::program_options;
    typedef std::vector<std::string> stringlist;

    std::string config_file_name;

    options_description generic("Generic Options");
    generic.add_options()
        ("help,h", "Display this help message and exit")
        ("version,v", "Display tversion information and exit")
        ("config,C", value<string>(&config_file_name)->default_value("cl.conf"), "Set config file")
        ("no-config", "Disable config file parsing")
    ;

    options_description linker("Linker Options");
    linker.add_options()
        ("link", value<stringlist>()->composing(), "Link specified object file")
        ("library,l", value<stringlist>()->composing(), "link Library")
        ("library-path,L", value<stringlist>()->composing(), "Add search path for -l")
    ;

    options_description config("Configuration Options");
    config.add_options()
        ("debug,g", "Enable output for debug information")
        ("include-path,I", value<stringlist>()->composing(), "Add include path")
        ("output,o", value<std::string>()->default_value("a.out"), "Output file name")
        ("compile,c", "Compile and assemble but don't link")
        ("compile-only,S", "Compile but don't assemble")
        ("run,r", "Run produced executable ( only in effect if -c and -S are not set )")
        ("verbosity,V",value<int>()->default_value(10),
                    "Control message output of the compiler:"
                    "\n  0: \tquiet mode, suppress all output"
                    "\n  1: \tdisplay only errors"
                    "\n  2: \tdisplay errors and warnings"
                    "\n  3: \tdisplay task actions (eg: 'Compiling file.cl')" )

    ;

    options_description not_in_help("");
    not_in_help.add_options()
        ("file", value<stringlist>()->composing(), "source file")
    ;

    options_description cmd_options;
    cmd_options.add(generic).add(config).add(linker).add(not_in_help);

    options_description config_options;
    config_options.add(config).add(linker).add(not_in_help);

    options_description helpable("Usage: cl [options] file...\nOptions");
    helpable.add(generic).add(config).add(linker);

    /*options_description desc("Usage: cl [options] file...\nOptions");
    desc.add_options()
        ("help,h", "display this help message and exit")
        ("version,v", "display tversion information and exit")
        ("debug,g", "enable output for debug information")
        ("error-level,e", value<int>()->default_value(0), "suppress compiler errors below that level")
        ("include-path,I", value<stringlist>(), "add include path")
        ("file", value<stringlist>(), "source file")
    ;*/
    positional_options_description p;
    p.add("file", -1);

    variables_map vm;
    store ( command_line_parser(argc, argv).options(cmd_options).positional(p).run(), vm );
    notify(vm);

    if ( !vm.count("no-config") )
    {
        std::ifstream conf_file ( config_file_name.c_str() );
        if ( conf_file )
        {
            store(parse_config_file(conf_file, config_options), vm);
            notify(vm);
        }
        else std::cerr << "Unable to open config file " << config_file_name << '\n';
    }

    if (vm.count("help"))
    {
        std::cout << helpable << "\n";
    }
    else if ( vm.count("version") )
    {
        std::cout << "CrapLang 0.0\n";
    }
    else if ( vm.count("file") == 0)
    {
        std::cerr << "No input files\n";
        return 1;
    }
    else
    {
        /// \todo include-path
        /// \todo verbosity
        /// \todo -o for -c and -S
        /// \todo --quiet

        bool debug = vm.count("debug");

        const stringlist &srcfiles = vm["file"].as<stringlist>();
        for ( unsigned i = 0; i < srcfiles.size(); i++ )
        {
            std::cout << "Compiling " << srcfiles[i] << "\n";
            compile_file(srcfiles[i],srcfiles[i]+".s",debug);
        }

        if ( !vm.count("S") )
        {

            stringlist link;
            if ( vm.count("link") )
                link = vm["link"].as<stringlist>();

            for ( unsigned i = 0; i < srcfiles.size(); i++ )
            {
                std::cout << "Assembling " << srcfiles[i] << "\n";
                assemble_file(srcfiles[i]+".s",srcfiles[i]+".o",debug);
                if ( !vm.count("c") )
                    link.push_back(srcfiles[i]+".o");

            }


            if ( !vm.count("c") )
            {
                const std::string& outfile = vm["output"].as<std::string>();
                std::cout << "Linking " << outfile << '\n';

                std::string linkopt;
                if ( vm.count("library") )
                {
                    const stringlist &libs = vm["library"].as<stringlist>();
                    for ( unsigned i = 0; i < libs.size(); i++ )
                    {
                        linkopt += " -l"+libs[i];
                    }
                }
                if ( vm.count("library-path") )
                {
                    const stringlist &libs = vm["library-path"].as<stringlist>();
                    for ( unsigned i = 0; i < libs.size(); i++ )
                    {
                        linkopt += " -L"+libs[i];
                    }
                }

                link_files(link,outfile, linkopt);

                if ( vm.count("run") )
                {
                    std::cout << "Running program\n\n";
                    int exit = launch_command("./"+outfile);
                    std::cout << "\nProgram returned " << exit << '\n';
                }
            }

        }


        /*std::cout << "Great!\n";

        if ( vm.count("include-path") != 0 )
        {
            std::cout << "Include path:\n";
            const stringlist &incpath = vm["include-path"].as<stringlist>();
            for ( unsigned i = 0; i < incpath.size(); i++ )
                std::cout << "    " << incpath[i] << '\n';
        }


        if ( vm.count("link-path") != 0 )
        {
            std::cout << "Link path:\n";
            const stringlist &incpath = vm["link-path"].as<stringlist>();
            for ( unsigned i = 0; i < incpath.size(); i++ )
                std::cout << "    " << incpath[i] << '\n';
        }

        if ( vm.count("link") != 0 )
        {
            std::cout << "Link to:\n";
            const stringlist &incpath = vm["link"].as<stringlist>();
            for ( unsigned i = 0; i < incpath.size(); i++ )
                std::cout << "    " << incpath[i] << '\n';
        }

        std::cout << "Source files:\n";
        const stringlist &srcfiles = vm["file"].as<stringlist>();
        for ( unsigned i = 0; i < srcfiles.size(); i++ )
            std::cout << "    " << srcfiles[i] << '\n';

        std::cout << "Output file: " << vm["output"].as<std::string>() << '\n';

        std::cout << "Error Level: " << vm["error-level"].as<int>() << '\n';

        if ( vm.count("debug") )
            std::cout << "Debug on\n";

        if ( vm.count("no-config") )
            std::cout << "Config file disabled\n";
        else
            std::cout << "Config File: " << config_file_name << '\n';

        if ( vm.count("S") )
            std::cout << "Compile only\n";
        else if ( vm.count("c") )
            std::cout << "Compile and assemble\n";
        else
            std::cout << "Compile, assemble and link\n";


        std::cout << helpable << '\n';*/
    }
}

void test_compiler()
{
    stringstream ss (
"function print ( string s ) \
{ \
    asm(' \n\
push 8(%rsp) #move argument in front of the stack \n\
call strlen \n\
popq %rsi \n\
mov %rax, %rdx \n\
movq $1, %rax  \n\
movq $1, %rdi  \n\
syscall  \n\
    '); \
} \
function int strlen ( string s ) \
{ \
    asm(' \n\
movq 8(%rsp), %rdi  \n\
xor %rcx, %rcx   \n\
not %rcx # %rcx = -1 = max  \n\
sub %al, %al # %al = \\\\0 (terminator)  \n\
cld # setup flags for string operation  \n\
repne scasb # find %al in %rdi, decrease %rcx each time  \n\
not	%rcx # %rcx = -%rcx = size+1  \n\
dec %rcx # %rcx = size  \n\
mov %rcx, %rax # return size  \n\
    '); \
} \
print ( 'Hello World!\\n' ); \
$test = 1+2*3; \
exit 2*7+(test-4)-17; \
" );

    parser p(ss,x86_64_gas_generator(cout),error_handler(cerr),true);
    //parser p(ss,three_address_code_generator(cout),cerr);
    p.parse();

}

void test_parser()
{
    stringstream ss (
"function int bar ( int x ); \
function foo \
    bar(0); \
function int bar ( int x ) \
{ \
    $y = 'foo bar'; \
    if ( x != 5 ) \
      bar(5); \
    return x+4*2; \
} \
\
$x = bar(1); \
"
 /*" \
 ; \
 $y = 1.0; \
 $x = 0; \
 while ( x < 5 ) \
 { \
    $x = x+1; \
    if ( y-x > 5 ) \
        $x = 4*y+2; \
    else if ( y )\
        $y = (x+1)/2; \
 } \
  "*/
// "1*(2/3.) and -4 + f(5+6,7*8) * --g(9,) -- h();"
/*" \
if ( if1 ) $x = inif1; $x = outif1; \
if ( if2 ) { $x = inif2; $y = inif2; } $x = outif2; \
while ( while1 ) $x = inwhile1; $x = outwhile1; \
if ( if3 ) \
{ \
    if ( if4inif3 ) \
        $x = inif4; \
    else \
        $x = inelse4; \
    $x = inif3; \
} \
else if ( elseif3 ) \
    $x = inelseif3; \
else \
    $x = inelse3; \
$x = outif3; \
"*/
 );
    parser p(ss,three_address_code_generator(cout),error_handler(cerr),true);
    p.parse();
}
