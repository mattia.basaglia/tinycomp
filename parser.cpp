#include "parser.hpp"
#include <fstream>
#ifdef EOF
#   undef EOF
#endif

void parser::parse()
{
    scan();
    if ( debug )
        gen->debug_file ( file, file_counter );
    gen->text();
    bool executed = false;
    if ( la == "main" ) /// \todo remove this?
    {
        executed = true;
        scan();
        gen->start();
    }

    while ( la != lexer::EOF )
        statement(NOTHING, label(), label() );
    if ( executed )
        gen->exit();
    gen->data();
}

void parser::debug_line()
{
    if ( debug )
        gen->debug_line ( lex.line, file_counter );
}

/**
    \pre lookahead already read
    \post lookahead contains next token
*/
label parser::statement ( breakable where, label loopcontinue, label loopbreak )
{
    if ( la == "while" )
    {
        debug_line();
        return s_while ( where );
    }
    else if ( la == "if" )
    {
        debug_line();
        return s_if ( where, loopcontinue, loopbreak );
    }
    else if ( la == "{" )
    {
        return block ( where, loopcontinue, loopbreak );
    }
    else if ( la == "$" )
    {
        debug_line();
        return assign();
    }
    else if ( la == ";" )
    {
        scan();
        return "";
    }
    else if ( la == "function" )
        return function_decl();
    else if ( la == "exit" )
    {
        debug_line();
        scan();
        label t = expression();
        if ( la != ";" )
            err->error(file,lex.line,"Expected ;",1);
        else
            scan();
        cast_to ( t, INTEGER );
        gen->exit(t);
        return t;
    }
    else if ( la == "include" )
    {
        scan();
        std::string oldname = file;
        file = la.str;
        if ( la.type != lexer::STRING )
            err->error ( file, lex.line, "Expected string for file name", 0 );

        std::streambuf *oldbuf = lex.rdbuf();
        std::ifstream included(file.c_str());
        lex.rdbuf(included.rdbuf());
        file_counter++;
        unsigned line = lex.line;
        lex.line = 1;
        if ( debug )
        {
            gen->debug_file ( file, file_counter );
            debug_line();
        }

        scan();
        do
            statement(where, loopcontinue, loopbreak);
        while ( la != lexer::EOF );

        file_counter--;
        lex.rdbuf(oldbuf);
        file = oldname;
        lex.line = line;
        if ( debug )
        {
            gen->debug_file ( file, 0 );
            debug_line();
        }
        scan();

        return "";
    }
    else if ( la == "asm" ) // inline assembly
    {
        debug_line();
        scan();

        if ( la != "(" )
            err->error(file,lex.line,"Expected (",1);
        else
            scan();

        if ( la != lexer::STRING )
            err->error(file,lex.line,"Expected string",1);
        gen->raw(la.str);
        scan();

        if ( la != ")" )
            err->error(file,lex.line,"Expected )",1);
        else
            scan();

        if ( la != ";" )
            err->error(file,lex.line,"Expecting ;",1);
        else
            scan();

        return "";
    }
    else if ( (where & FUNCTION) && la == "return" )
    {
        debug_line();
        scan();
        if ( la != ";" )
        {
            label retv = expression();
            gen->f_return ( retv );
            if ( la != ";" )
                err->error(file,lex.line,"Expected ;",1);
        }
        else
            gen->f_return();
        scan();
        return "";
    }
    else if ( where&LOOP )
    {
        if ( la == "break" )
        {
            debug_line();
            scan();
            if ( la != ";" )
                err->error(file,lex.line,"Expected ;",1);
            else
                scan();
            gen->jump ( loopbreak );
            return "";
        }
        else if ( la == "continue" )
        {
            debug_line();
            scan();
            if ( la != ";" )
                err->error(file,lex.line,"Expected ;",1);
            else
                scan();
            gen->jump ( loopcontinue );
            return "";
        }
    }

    debug_line();
    label e = expression(); /// \bug why not expecting ';' here? o_O
    if ( la != ";" )
        err->error ( file, lex.line, "Expected ;", 1 );
    else
        scan();
    return e;
}

/**
    \pre lookahead is "function"
    \post lookahead contains next token
*/
label parser::function_decl()
{
    scan();
    ValueType ret_type = name_type(la.str);

    if ( ret_type != VOID )
        scan();

    function fid( ret_type );
    if ( la != lexer::IDENTIFIER )
    {
        err->error(file,lex.line,"Unnamed function",1);
        fid.name = label::position().name;
    }
    else
        fid.name = la.str;

    scan();
    if ( la == "(" )
    {
        scan();
        if ( la != ")" )
        {
            param_list(fid);
            if ( la != ")" )
                err->error(file,lex.line,"Unmatched )",1);
        }
        scan();
    }

    if ( symbol_table.is_func(fid.name) )
    {
        function f = symbol_table.get_func(fid.name);
        if ( f.type != fid.type || f.parameters != fid.parameters )
            err->error(file,lex.line,"Redeclaration of "+fid.name,1);
    }

    symbol_table.declare_func(fid);

    if ( la != ";" ) // implementation
    {
        debug_line();
        gen->enter_function(fid);

        symbol_table.push_scope();
        for ( std::vector<label>::const_iterator p = fid.parameters.begin();
                p != fid.parameters.end(); ++p )
            symbol_table.declare_var(*p);

        statement(FUNCTION, label(), label());

        symbol_table.pop_scope();

        gen->leave_function();
    }
    else // forward declaration
        scan();

    return fid;

}

void parser::param_list( function& f )
{
    while ( la != ")" )
    {
        if ( la != lexer::IDENTIFIER )
        {
            err->error(file,lex.line,"Expected type name",1);
            scan();
        }
        else if ( name_type(la.str) == VOID )
        {
            err->error(file,lex.line,"Expected type name (int assumed)",0);
            f.parameters.push_back ( label(la.str,INTEGER ) );
        }
        else
        {
            label p;
            p.type = name_type(la.str);
            scan();
            if ( la != lexer::IDENTIFIER )
                err->error(file,lex.line,"Expected parameter identifier",1);
            else
            {
                p.name = la.str;
                f.parameters.push_back ( p );
            }
            scan();
            /// \todo default arguments
            if ( la == "," )
                scan();
        }
    }
}

/**
    \pre lookahead is $
    \post lookahead contains next token
*/
label parser::assign()
{
    scan();
    while ( la.type != lexer::IDENTIFIER )
    {
        err->error(file,lex.line,"Expected identifier after $",1);
        scan();
    }
    label id = la.str;
    scan();
    if ( la != "=" )
        err->error(file,lex.line,"Expected initializer",1);
    scan();
    label val = expression();

    if ( !symbol_table.is_var(id.name, true) )
    {
        id.type = val.type;
        symbol_table.declare_var(id);
    }
    else
    {
        err->error(file,lex.line,"Redeclaration treated as assignment",0);
        id = symbol_table.get_var(id.name);
        cast_to ( val, id.type );
    }

    gen->assign ( id, val );

    if ( la != ";" )
        err->error(file,lex.line,"Expected ; after declaration",1);
    else
        scan();

    return id;
}

/**
    \pre lookahead is 'while'
    \post lookahead contains next token
*/
label parser::s_while ( breakable where )
{
    scan();
    if ( la != "(" )
    {
        err->error(file,lex.line,"Expected ( after 'while'",1);
    }
    label while_top = label::position("while condition");
    label while_bottom = label::position("end while");
    gen->place_label ( while_top );
    scan();
    label cond = expression();
    if ( la != ")" )
        err->error(file,lex.line,"Expected ) after 'while' condition",1);
    scan();
    cast_to ( cond, INTEGER );
    gen->jump_if(cond,while_bottom,true);
    statement(breakable(where|LOOP), while_top, while_bottom );
    gen->jump ( while_top );
    gen->place_label ( while_bottom );
    return while_bottom;
}

/**
    \pre lookahead is 'if'
    \post lookahead contains next token
*/
label parser::s_if ( breakable where, label loopcontinue, label loopbreak  )
{
    scan();
    if ( la != "(" )
    {
        err->error(file,lex.line,"Expected ( after 'if'",1);
    }
    label if_else = label::position("else");
    label if_bottom = label::position("end if");
    scan();
    label cond = expression();
    cast_to ( cond, INTEGER );
    gen->jump_if ( cond, if_else, true );
    if ( la != ")" )
        err->error(file,lex.line,"Expected ) after 'if' condition",1);
    scan();
    statement(where,loopcontinue, loopbreak);
    gen->jump ( if_bottom );
    gen->place_label ( if_else );
    if ( la == "else" )
    {
        scan();
        statement(where, loopcontinue, loopbreak);
    }
    gen->place_label ( if_bottom );
    return if_bottom;
}

/**
    \pre lookahead is {
    \post lookahead contains next token
*/
label parser::block ( breakable where, label loopcontinue, label loopbreak  )
{
    label l;
    scan();
    symbol_table.push_scope();
    gen->enter_scope();
    while ( true )
    {
        if ( la == "}" )
        {
            scan();
            break;
        }
        else if ( la == lexer::EOF )
        {
            err->error(file,lex.line,"Expected closing }",1);
            break;
        }
        l = statement(where, loopcontinue, loopbreak);
    }
    symbol_table.pop_scope();
    gen->leave_scope();
    return l;
}


/**
    \pre lookahead already read
    \post lookahead contains next token
*/
label parser::expression()
{
    return assign_expr();
}
label parser::assign_expr()
{
    label a = bool_expr();
    if ( a.reference || a.special == label::NONE )
    {
        if ( la == "=" )
        {
            scan();
            label b = bool_expr();
            if ( !a.reference && !symbol_table.is_var(a.name) )
            {
                a.type = b.type;
                symbol_table.declare_var(a);
            }
            gen->assign ( a, b );
        }
        else if ( la.str.size() == 2 && la.str[1] == '=' )
        {
            char op = la.str[0];
            scan();
            label b = bool_expr();
            gen->assign ( a, a, b, std::string(1,op) );
        }
    }
    return a;
};

label parser::bool_expr()
{
    label a = comp_expr();
    while ( true )
    {
        if ( la == "and" ) /// \todo short-circuit
        {
            scan();
            label b = comp_expr();
            cast_to(a,INTEGER);
            cast_to(b,INTEGER);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, "&&" );
            a = t;
        }
        else if ( la == "or" )
        {
            scan();
            label b = comp_expr();
            cast_to(a,INTEGER);
            cast_to(b,INTEGER);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, "||" );
            a = t;
        }
        else break;
    }
    return a;
}

label parser::comp_expr()
{
    label a = arith_expr();
    while ( true )
    {
        if ( la == "==" )
        {
            scan();
            label b = arith_expr();
            fix_types(a,b);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, "==" );
            a = t;
        }
        else if ( la == ">=" )
        {
            scan();
            label b = arith_expr();
            fix_types(a,b);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, ">=" );
            a = t;
        }
        else if ( la == "<=" )
        {
            scan();
            label b = arith_expr();
            fix_types(a,b);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, "<=" );
            a = t;
        }
        else if ( la == "!=" )
        {
            scan();
            label b = arith_expr();
            fix_types(a,b);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, "!=" );
            a = t;
        }
        else if ( la == ">" )
        {
            scan();
            label b = arith_expr();
            fix_types(a,b);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, ">" );
            a = t;
        }
        else if ( la == "<" )
        {
            scan();
            label b = arith_expr();
            fix_types(a,b);
            label t = label::temp(INTEGER);
            gen->assign ( t, a, b, "<" );
            a = t;
        }
        else break;
    }
    return a;
}

label parser::arith_expr()
{
    label a = term_expr();
    while ( true )
    {
        if ( la == "+" )
        {
            scan();
            label b = term_expr();
            fix_types(a,b);
            label t = label::temp(max_type(a.type,b.type));
            gen->assign ( t, a, b, "+" );
            a = t;
        }
        else if ( la == "-" )
        {
            scan();
            label b = term_expr();
            fix_types(a,b);
            label t = label::temp(max_type(a.type,b.type));
            gen->assign ( t, a, b, "-" );
            a = t;
        }
        else break;
    }
    return a;
}

label parser::term_expr()
{
    label a = fact_expr();
    while ( true )
    {
        if ( la == "*" )
        {
            scan();
            label b = fact_expr();
            fix_types(a,b);
            label t = label::temp(max_type(a.type,b.type));
            gen->assign ( t, a, b, "*" );
            a = t;
        }
        else if ( la == "/" )
        {
            scan();
            label b = fact_expr();
            fix_types(a,b);
            label t = label::temp(max_type(a.type,b.type));
            gen->assign ( t, a, b, "/" );
            a = t;
        }
        else if ( la == "%" )
        {
            scan();
            label b = fact_expr();
            label t = label::temp(INTEGER);
            cast_to(a,INTEGER);
            cast_to(b,INTEGER);
            gen->assign ( t, a, b, "%" );
            a = t;
        }
        else break;
    }
    return a;
}

label parser::fact_expr()
{
    label a = prim_expr();
    if ( a.special == label::NONE && la == "[" )
    {
        scan();
        label b = expression();
        if ( la != "]" )
            err->error(file,lex.line,"Expected ]",1);
        else
            scan();
        cast_to(b,INTEGER);
        label t = label::temp(a.type);
        gen->assign ( t, a, b, "+" );
        t.reference = true;
        return t;
    }
    return a;
}

label parser::prim_expr()
{

    if ( la == "-" )
    {
        scan();
        label a = fact_expr();
        label t = label::temp(a.type);
        gen->assign ( t, a, "-" );
        return t;
    }
    else if ( la == "*" )
    {
        scan();
        label a = fact_expr();
        a.reference = true;
        //gen->assign ( t, a, "*" );
        return a;
    }
    else if ( la == "$" )
    {
        scan();
        label a = fact_expr();
        label t = label::temp(a.type);
        gen->assign ( t, a, "&" );
        return t;
    }
    else if ( la == "not" )
    {
        scan();
        label a = fact_expr();
        label t = label::temp(a.type);
        gen->assign ( t, a, "!" );
        return t;
    }
    else if ( la == "(" )
    {
        scan();
        label t = expression();
        if ( la != ")" )
            err->error(file,lex.line,"Expecting )",1);
        else
            scan();
        return t;
    }
    else if ( la == lexer::IDENTIFIER )
    {
        label id = la.str;

        scan();

        if ( la == "(" ) // function call
        {
            if ( symbol_table.is_func(id.name) )
            {
                const function &fn = symbol_table.get_func(id.name);

                gen->begin_parameters();
                arg_list(fn);

                label t = label::temp(fn.type);
                gen->call ( t, fn );
                return t;
            }
            else if ( symbol_table.is_var(id.name) )
            {
                err->error(file,lex.line,"Using variable as function",1);
                skip_to_rparen();
                scan();
                return symbol_table.get_var(id.name);
            }
            else
            {
                /*err->error(file,lex.line,"Calling undeclared function "+id.name+", arguments will be ignored",0);
                skip_to_rparen(); /// \todo maybe it can be worked out
                scan();
                label t = label::temp(VOID);
                gen->call ( t, function(VOID,id.name) );*/
                function fn(INTEGER,id.name);
                err->error(file,lex.line,"Calling undeclared function "+id.name,0);
                gen->begin_parameters();
                unchecked_arg_list(fn);
                label t = label::temp(fn.type);
                gen->call ( t, fn );

                return t;
            }
        }


        if ( symbol_table.is_var(id.name) )
            id = symbol_table.get_var(id.name);
        else if ( symbol_table.is_func(id.name) )
        {
            err->error(file,lex.line,"Using function as value",1);
            label t = label::temp(id.type);
            gen->call ( t, symbol_table.get_func(id.name) );
            return t;
        }
        else
        {
            err->error(file,lex.line,"Using undeclared variable",0);
            id.type = INTEGER;
        }

        return id;
    }
    else if ( la == lexer::INTEGER )
    {
        label n = label::int_literal( la.str );
        scan();
        return n;
    }
    else if ( la == lexer::FLOAT )
    {
        label n ( la.str, FLOAT ); /// \todo literal table
        scan();
        return n;
    }
    else if ( la == lexer::STRING )
    {
        label n = symbol_table.declare_str(la.str);
        scan();
        return n;
    }
    else if ( la == lexer::CHAR )
    {
        std::ostringstream ss;
        ss << int(la.str[0]);
        label n = label::int_literal(ss.str());
        scan();
        return n;
    }
    else
    {
        err->error(file,lex.line,"Expected primary expression",1);
        scan(); /// avoids loops in expression
        return label("0",INTEGER);
    }
}

/**
    \pre symbol_table.is_func(name) == true && la == "("
*/
void parser::arg_list(const function& f)
{
    scan();
    for ( std::vector<label>::const_iterator i = f.parameters.begin();
            i != f.parameters.end(); ++i )
    {
        label a = expression();
        gen->push_parameter ( f, *i, a );
        if ( la == ")" )
        {
            if ( i != f.parameters.end()-1 )
            {
                err->error(file,lex.line,"Too few argumets passed to "+f.name,1);
                return;
            }
        }
        else if ( la != "," )
            err->error(file,lex.line,"Expecting ,",1);
        else
            scan();
    }
    if ( la != ")" )
    {
        err->error(file,lex.line,"Too many arguments passed to "+f.name,1);
        skip_to_rparen();
    }
    scan();
    /*while ( la != ")" )
    {
        label a = expression();
        out << "push " << a << '\n';
        if ( la == "," )
            scan();
    }*/
}

/**
    \pre symbol_table.is_func(name) == false && la == "("
    \todo can join w/ above and check symbol_table.is_func(name) -> issue warning here

*/
void parser::unchecked_arg_list( function& f )
{

    scan();
    while ( la != ")" )
    {
        label a = expression();
        f.parameters.push_back ( a );
        gen->push_parameter ( f, a, a );
        if ( la != "," )
            err->error(file,lex.line,"Expecting ,",1);
        else if ( la == ")" )
            break;
        else
            scan();
    }
    scan();
}

/**
    \post la == ")"
*/
void parser::skip_to_rparen()
{
    while ( la != ")" )
        scan();
}



void parser::fix_types ( label& a, label& b )
{
    ValueType t = max_type ( a.type, b.type );
    cast_to ( a, t );
    cast_to ( b, t );
}
void parser::cast_to ( label& l, ValueType t )
{
    if ( l.type != t && gen->willcast(t,l.type) )
    {
        label temp = label::temp(t);
        gen->cast ( temp, l );
        l = temp;
    }
}
ValueType parser::max_type(ValueType a, ValueType b)
{
    if ( a == FLOAT || b == FLOAT )
        return FLOAT;
    else if ( a == STRING || b == STRING )
        return STRING;
    else if ( a == INTEGER || b == INTEGER )
        return INTEGER;
    else
        return VOID;
}

