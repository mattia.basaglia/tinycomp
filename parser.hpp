#ifndef PARSER_HPP_
#define PARSER_HPP_

#include <iostream>
#include <sstream>
#include <map>
#include <boost/shared_ptr.hpp>
#include "lexer.hpp"
#include "symbol_table.hpp"
#include "generic_generator.hpp"
#include "error_handler.hpp"

class parser
{
    protected:
    //{ types
        enum breakable /// jumpable statement ( nothing, function=>return, loop=>break )
        {
            NOTHING = 0,
            FUNCTION = 1,
            LOOP = 2,
            LOOPFUN = FUNCTION|LOOP,
        };
    //}
    //{ properties
        lexer lex;
        boost::shared_ptr<generic_generator> gen;
        boost::shared_ptr<generic_error_handler> err;
        lexer::token la;
        SymbolTable symbol_table;
        bool debug;
        std::string file; ///< Used for error output / debug symbols
        unsigned file_counter;

        void debug_line();
    //}

    public:
        template<class GEN, class ERR>
            parser ( std::istream& in,
                    const GEN& rgen,
                    const ERR& err,
                    bool debug,
                    const std::string &file = "-",
                    unsigned file_counter = 1 )
            : lex(in), gen (new GEN(rgen)),
            err(new ERR(err)), debug(debug), file(file), file_counter(file_counter)
        {
            gen->pst = &symbol_table; /// \todo copy constructor
        }
        virtual ~parser()
        {}

        void parse();


    protected:


        /**
            \pre lookahead already read
            \post lookahead contains next token
        */
        label statement ( breakable where, label loopcontinue, label loopbreak );
        /**
            \pre lookahead is "function"
            \post lookahead contains next token
        */
        label function_decl();

        void param_list( function& f );
        /**
            \pre lookahead is $
            \post lookahead contains next token
        */
        label assign();

        /**
            \pre lookahead is 'while'
            \post lookahead contains next token
        */
        label s_while ( breakable where );
        /**
            \pre lookahead is 'if'
            \post lookahead contains next token
        */
        label s_if ( breakable where, label loopcontinue, label loopbreak  );

        /**
            \pre lookahead is {
            \post lookahead contains next token
        */
        label block ( breakable where, label loopcontinue, label loopbreak  );

        /**
            \pre lookahead already read
            \post lookahead contains next token
        */
        label expression();

        label assign_expr();

        label bool_expr();

        label comp_expr();

        label arith_expr();

        label term_expr();

        label fact_expr();

        label prim_expr();

        /**
            \pre symbol_table.is_func(name) == true && la == "("
        */
        void arg_list(const function& name);
        void unchecked_arg_list( function& f );
        /**
            \post la == ")"
        */
        void skip_to_rparen();



        void fix_types ( label& a, label& b );

        void cast_to ( label& l, ValueType t );

        ValueType max_type(ValueType a, ValueType b); /// \todo move in symbol_table.hpp

        void scan()
        {
            la = lex.scan();
        }

};

#endif // PARSER_HPP_
