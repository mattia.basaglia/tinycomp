#include <iostream>
#include <sstream>
#include <map>
#include "lexer.hpp"
#include "symbol_table.hpp"
class parser
{
    protected:
    //{ types
        enum breakable /// jumpable statement ( nothing, function=>return, loop=>break )
        {
            NOTHING = 0,
            FUNCTION = 1,
            LOOP = 2,
            LOOPFUN = FUNCTION|LOOP,
        };
    //}
    //{ properties
        lexer lex;
        std::ostream& out;
        std::ostream& err;
        lexer::token la;
        std::map<std::string,SymbolType> symbol_table;
    //}

    public:
        parser(std::istream& in, std::ostream& out, std::ostream& err)
            : lex(in), out (out), err(err) {}

        void parse()
        {
            scan();
            while ( la != lexer::EOF )
                statement(NOTHING, label(), label() );
        }

    protected:


        /**
            \pre lookahead already read
            \post lookahead contains next token
        */
        label statement ( breakable where, label loopcontinue, label loopbreak )
        {
            if ( la == "while" )
                return s_while ( where );
            else if ( la == "if" )
                return s_if ( where, loopcontinue, loopbreak );
            else if ( la == "{" )
            {
                return block ( where, loopcontinue, loopbreak );
            }
            else if ( la == "$" )
                return assign();
            else if ( la == ";" )
            {
                scan();
                return "";
            }
            else if ( la == "function" )
                return function();
            else if ( (where & FUNCTION) && la == "return" )
            {
                scan();
                if ( la != ";" )
                {
                    label retv = expression();
                    out << "return " << retv << '\n';
                    if ( la != ";" )
                        err << "Expected ;\n";
                }
                else
                    out << "return\n";
                scan();
                return "";
            }
            else if ( where&LOOP )
            {
                if ( la == "break" )
                {
                    scan();
                    if ( la != ";" )
                        err << "Expected ;\n";
                    else
                        scan();
                    out << "goto " << loopbreak << '\n';
                    return "";
                }
                else if ( la == "continue" )
                {
                    scan();
                    if ( la != ";" )
                        err << "Expected ;\n";
                    else
                        scan();
                    out << "goto " << loopcontinue << '\n';
                    return "";
                }
            }

            return expression();
        }

        /**
            \pre lookahead is "function"
            \post lookahead contains next token
        */
        label function()
        {
            label skip = label::position("Skip function body");
            scan();
            ValueType ret_type;
            if ( la == "int" )
            {
                ret_type = INTEGER;
                scan();
            }
            else if ( la == "float" )
            {
                ret_type = FLOAT;
                scan();
            }
            else if ( la == "string" )
            {
                ret_type = STRING;
                scan();
            }
            else
                ret_type = VOID;

            label fid( "", ret_type, "function" );
            if ( la != lexer::IDENTIFIER )
            {
                err << "Unnamed function\n";
                fid.name = label::position().name;
            }
            else
                fid.name = la.str;

            scan();
            if ( la == "(" )
            {
                scan();
                if ( la != ")" )
                {
                    param_list();
                    if ( la != ")" )
                        err << "Unmatched )\n";
                }
                scan();
            }

            symbol_table[fid.name] = SymbolType(ret_type,true);

            out << "goto " << skip << '\n';
            out << fid << " :\n";

            statement(FUNCTION, label(), label());

            out << "return\n";
            out << skip << ":\n";

            return fid;

        }
        void param_list()
        {
            /// \todo
        }

        /**
            \pre lookahead is $
            \post lookahead contains next token
        */
        label assign()
        {
            scan();
            while ( la.type != lexer::IDENTIFIER )
            {
                err << "Expected identifier after $\n";
                scan();
            }
            label id = la.str;
            scan();
            if ( la != "=" )
            {
                err << "Expected = in assignment\n";
            }
            scan();
            label val = expression();

            if ( symbol_table.count(id.name) == 0 )
                symbol_table[id.name] = val.type;
            else
            {
                id.type = symbol_table[id.name];
                cast_to ( val, id.type );
            }

            id.type = val.type;

            out << id << " = " << val << '\n'; /// \todo += etc
            if ( la != ";" )
                err << "Expected ; after assignment\n";
            else
                scan();

            return id;
        }

        /**
            \pre lookahead is 'while'
            \post lookahead contains next token
        */
        label s_while ( breakable where )
        {
            scan();
            if ( la != "(" )
            {
                err << "Expected ( after 'while'\n";
            }
            label while_top = label::position("while condition");
            label while_bottom = label::position("end while");
            out << while_top << ": \n";
            scan();
            label cond = expression();
            if ( la != ")" )
                err << "Expected ) after 'while' condition\n";
            scan();
            cast_to ( cond, INTEGER );
            out << "if not " << cond << " goto " << while_bottom << '\n';
            statement(breakable(where|LOOP), while_top, while_bottom );
            out << "goto " << while_top << '\n';
            out << while_bottom << ": \n";
            return while_bottom;
        }

        /**
            \pre lookahead is 'if'
            \post lookahead contains next token
        */
        label s_if ( breakable where, label loopcontinue, label loopbreak  )
        {
            scan();
            if ( la != "(" )
            {
                err << "Expected ( after 'if'\n";
            }
            label if_else = label::position("else");
            label if_bottom = label::position("end if");
            scan();
            label cond = expression();
            cast_to ( cond, INTEGER );
            out << "if not " << cond << " goto " << if_else << '\n';
            if ( la != ")" )
                err << "Expected ) after 'if' condition\n";
            scan();
            statement(where,loopcontinue, loopbreak);
            out << "goto " << if_bottom << '\n';
            out << if_else << ": \n";
            if ( la == "else" )
            {
                scan();
                statement(where, loopcontinue, loopbreak);
            }
            out << if_bottom << ": \n";
            return if_bottom;
        }

        /**
            \pre lookahead is {
            \post lookahead contains next token
        */
        label block ( breakable where, label loopcontinue, label loopbreak  )
        {
            label l;
            scan();
            while ( true )
            {
                l = statement(where, loopcontinue, loopbreak);
                if ( la == "}" )
                {
                    scan();
                    break;
                }
                else if ( la == lexer::EOF )
                {
                    err << "Expected closing }\n";
                    break;
                }
            }
            return l;
        }


        /**
            \pre lookahead already read
            \post lookahead contains next token
        */
        label expression()
        {
            return bool_expr();
            /*if ( look )
                scan();
            std::string v = la.str;
            scan();
            out << "evaluating t = " << v << "\n";
            return "t";*/
        }

        label bool_expr()
        {
            label a = comp_expr();
            while ( true )
            {
                if ( la == "and" )
                {
                    scan();
                    label b = comp_expr();
                    cast_to(a,INTEGER);
                    cast_to(b,INTEGER);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " && " << b << '\n';
                    a = t;
                }
                else if ( la == "or" )
                {
                    scan();
                    label b = comp_expr();
                    cast_to(a,INTEGER);
                    cast_to(b,INTEGER);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " || " << b << '\n';
                    a = t;
                }
                else break;
            }
            return a;
        }

        label comp_expr()
        {
            label a = arith_expr();
            while ( true )
            {
                if ( la == "==" )
                {
                    scan();
                    label b = arith_expr();
                    fix_types(a,b);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " == " << b << '\n';
                    a = t;
                }
                else if ( la == ">=" )
                {
                    scan();
                    label b = arith_expr();
                    fix_types(a,b);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " >= " << b << '\n';
                    a = t;
                }
                else if ( la == "<=" )
                {
                    scan();
                    label b = arith_expr();
                    fix_types(a,b);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " <= " << b << '\n';
                    a = t;
                }
                else if ( la == "!=" )
                {
                    scan();
                    label b = arith_expr();
                    fix_types(a,b);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " != " << b << '\n';
                    a = t;
                }
                else if ( la == ">" )
                {
                    scan();
                    label b = arith_expr();
                    fix_types(a,b);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " > " << b << '\n';
                    a = t;
                }
                else if ( la == "<" )
                {
                    scan();
                    label b = arith_expr();
                    fix_types(a,b);
                    label t = label::temp(INTEGER);
                    out << t << " = " << a << " < " << b << '\n';
                    a = t;
                }
                else break;
            }
            return a;
        }

        label arith_expr()
        {
            label a = term_expr();
            while ( true )
            {
                if ( la == "+" )
                {
                    scan();
                    label b = term_expr();
                    fix_types(a,b);
                    label t = label::temp(max_type(a.type,b.type));
                    out << t << " = " << a << " + " << b << '\n';
                    a = t;
                }
                else if ( la == "-" )
                {
                    scan();
                    label b = term_expr();
                    fix_types(a,b);
                    label t = label::temp(max_type(a.type,b.type));
                    out << t << " = " << a << " - " << b << '\n';
                    a = t;
                }
                else break;
            }
            return a;
        }

        label term_expr()
        {
            label a = fact_expr();
            while ( true )
            {
                if ( la == "*" )
                {
                    scan();
                    label b = fact_expr();
                    fix_types(a,b);
                    label t = label::temp(max_type(a.type,b.type));
                    out << t << " = " << a << " * " << b << '\n';
                    a = t;
                }
                else if ( la == "/" )
                {
                    scan();
                    label b = fact_expr();
                    fix_types(a,b);
                    label t = label::temp(max_type(a.type,b.type));
                    out << t << " = " << a << " / " << b << '\n';
                    a = t;
                }
                else if ( la == "%" )
                {
                    scan();
                    label b = fact_expr();
                    label t = label::temp(INTEGER);
                    cast_to(a,INTEGER);
                    cast_to(b,INTEGER);
                    out << t << " = " << a << " % " << b << '\n';
                    a = t;
                }
                else break;
            }
            return a;
        }

        label fact_expr()
        {

            if ( la == "-" )
            {
                scan();
                label a = fact_expr();
                label t = label::temp(a.type);
                out << t << " = -" << a << '\n';
                return t;
            }
            else if ( la == "(" )
            {
                scan();
                label t = expression();
                if ( la != ")" )
                    err << "Expecting )\n";
                else
                    scan();
                return t;
            }
            else if ( la == lexer::IDENTIFIER )
            {
                label id = la.str; /// \todo symbol table

                if ( symbol_table.count(la.str) != 0 )
                    id.type = symbol_table[la.str];

                scan();

                if ( la == "(" ) // function call
                {
                    scan();
                    arg_list();
                    if ( la != ")" )
                        err << "Expecting )\n";
                    else
                        scan();

                    if ( symbol_table.count(id.name) == 0 )
                    {
                        err << "Warning: calling undeclared function\n";
                        id.type = VOID;
                    }
                    else if ( !symbol_table[id.name].function )
                    {
                        err << "Using variable as function\n";
                        return id;
                    }

                    label t = label::temp(id.type);
                    out << t << " = call " << id << '\n';
                    return t;
                }

                if ( symbol_table.count(id.name) == 0 )
                {
                    err << "Warning: using uninitialized variable\n";
                    id.type = INTEGER;
                }
                else if ( symbol_table[id.name].function )
                {
                    err << "Using function as value\n";
                    label t = label::temp(id.type);
                    out << t << " = call " << id << '\n';
                    return t;
                }

                return id;
            }
            else if ( la == lexer::INTEGER )
            {
                label n ( la.str, INTEGER );
                scan();
                return n;
            }
            else if ( la == lexer::FLOAT )
            {
                label n ( la.str, FLOAT ); /// \todo literal table
                scan();
                return n;
            }
            else if ( la == lexer::STRING )
            {
                label n ( la.str, STRING ); /// \todo literal table
                scan();
                return n;
            }
            else
            {
                err << "Expected primary expression\n";
                scan(); /// avoid loops in expression
                return label("0",INTEGER);
            }
        }

        void arg_list()
        {
            while ( la != ")" )
            {
                label a = expression();
                out << "push " << a << '\n';
                if ( la == "," )
                    scan();
            }
        }



        void fix_types ( label& a, label& b )
        {
            ValueType t = max_type ( a.type, b.type );
            cast_to ( a, t );
            cast_to ( b, t );
        }
        void cast_to ( label& l, ValueType t )
        {
            if ( l.type != t )
            {
                label temp = label::temp(t);
                std::string castfun = type_name(l.type)+'2'+type_name(t);
                out << temp << " = " << castfun << " ( " << l.name << " )\n";
                l = temp;
            }
        }
        ValueType max_type(ValueType a, ValueType b)
        {
            if ( a == FLOAT || b == FLOAT )
                return FLOAT;
            else if ( a == INTEGER || b == INTEGER )
                return INTEGER;
            else
                return VOID;
        }

        void scan()
        {
            la = lex.scan();
        }

};
