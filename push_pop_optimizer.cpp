#include "push_pop_optimizer.hpp"
#include <sstream>
void push_pop_optimizer::check ( std::istream&in, std::ostream&out )
{
    std::string line1, line2, instr, src, dest;
    std::istringstream line;
    while ( true )
    {
        std::getline(in,line1); /// \todo char delimiter in class?
        if ( !in )
            break;

        line.clear();
        line.str(line1);
        line >> instr;

        if ( instr != push )
        {
            out << line1 << '\n';
            continue;
        }
        line >> src;


        std::getline(in,line2); /// \todo char delimiter in class?
        if ( !in )
        {
            out << line1 << '\n';
            break;
        }

        line.clear();
        line.str(line2);
        line >> instr;

        if ( instr != pop )
        {
            out << line1 << '\n' << line2 << '\n';
            continue;
        }
        line >> dest;

        if ( src != dest )
            mov ( out, src, dest );

    }
}
