#ifndef PUSH_POP_OPTIMIZER_HPP_
#define PUSH_POP_OPTIMIZER_HPP_
#include <string>
#include <iostream>
#include <boost/function.hpp>
/**
    \class
    \brief optimizes redundant push and pops in a syntax-indipendent way

    If it detects a construct like
    \code
    push R1
    pop R2
    \endcode
    It will remove with a \b mov istruction or nothing if \$ R_1 = R_2 \$

    \note it uses std streams to determine if this occurs so the instruction should be like so:
    \code
    push (space) register [ (space) comment ] '\n'
    pop (space) register [ (space) comment ] '\n'
    \endcode
    Where \b push and \b pop are defined in push_pop_optimizer::push and push_pop_optimizer::pop.<br>
    These and \b register must not contain whitespaces.

    \note it is assumed that the following will never occur:
    \code
    push mem_location
    pop other_mem_location
    \endcode
*/
class push_pop_optimizer
{
    protected:
        std::string push, pop;

        /**
            \brief typedef for mov functor, called if push-pop refer to different registers
            \param ostream output
            \param string1 source
            \param string2 destination
            \example
            \code
            void mov ( std::ostream& o, const std::string& r1, const std::string& r2 )
            {
               // intel syntax
               o << "mov " << r2 << ' ' << r1 << '\n';
            }
            \endcode
            \note must output trailing newline ( if any is required )
        */
        typedef boost::function<void (std::ostream&,const std::string&,const std::string&)>
                    movfunc;
        movfunc mov;

    public:
        push_pop_optimizer( const std::string& push,
                            const std::string& pop,
                            const movfunc &mov )
            : push(push), pop(pop), mov(mov) {}

        void check ( std::istream&in, std::ostream&out );
};

#endif // PUSH_POP_OPTIMIZER_HPP_
