
function int strleng ( string s )
{
    asm ( '
    #movq 8(%rsp), %rdi
    xor %rcx, %rcx
    not %rcx # %rcx = -1 = max
    sub %al, %al # %al = \0 (terminator)
    cld # setup flags for string operation
    repne scasb # find %al in %rdi, decrease %rcx each time
    not	%rcx # %rcx = -%rcx = size+1
    dec %rcx # %rcx = size
    mov %rcx, %rax # return size
    addq $8, %rsp
    ret
    ' );
}

function print ( string s )
{
    asm ( '
    call strleng
    movq 0(%rsp), %rsi ##
    movq %rax, %rdx
    movq $1, %rax
    movq $1, %rdi
    syscall
    ' );
}

function reverse ( string s )
{
    $len = strleng(s)-1;
    $i = len/2;
    $t = 0;
    while ( i >= 0 )
    {
        t = s[i];
        s[i] = s[len-i];
        s[len-i] = t;
        i -= 1;
    }
}

function printnumber(int x)
{
    $string = "*********************";
    if ( x != 0 )
    {
        $y = x;
        if ( x < 0 )
            y = -x;
        $c = 0;
        while ( y )
        {
            string[c] = '0' + y % 10;
            y /= 10;
            c += 1;
        }
        if ( x < 0 )
        {
            string[c] = '-';
            c += 1;
        }
        string[c] = 0;
        reverse ( string );
        string[c] = '\n';
        string[c+1] = 0;
    }
    else
    {
        string[0] = '0';
        string[1] = '\n';
        string[2] = 0;
    }
    print ( string );
}
