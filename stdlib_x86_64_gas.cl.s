.file 1 "stdlib_x86_64_gas.cl"
.text
.loc 1 3 0
.loc 1 20 0
.loc 1 32 0
.loc 1 46 0

# Functions

.global strleng
.type strleng, @function
.global print
.type print, @function
.global reverse
.type reverse, @function
.global printnumber
.type printnumber, @function

strleng:
pushq %rdi
.loc 1 4 0
# inline assembly {

    #movq 8(%rsp), %rdi
    xor %rcx, %rcx
    not %rcx # %rcx = -1 = max
    sub %al, %al # %al = 0 (terminator)
    cld # setup flags for string operation
    repne scasb # find %al in %rdi, decrease %rcx each time
    not	%rcx # %rcx = -%rcx = size+1
    dec %rcx # %rcx = size
    mov %rcx, %rax # return size
    addq $8, %rsp
    ret
    
# }
xorq %rax, %rax
addq $8, %rsp
ret

print:
pushq %rdi
.loc 1 21 0
# inline assembly {

    call strleng
    movq 0(%rsp), %rsi ##
    movq %rax, %rdx
    movq $1, %rax
    movq $1, %rdi
    syscall
    
# }
xorq %rax, %rax
addq $8, %rsp
ret

reverse:
pushq %rdi
.loc 1 33 0
movq 0(%rsp), %rdi
call strleng
subq $1, %rax
pushq %rax #t_1__
# above is len
.loc 1 34 0
movq 0(%rsp), %rax
xorq %rdx, %rdx
movq $2, %rbx
idivq %rbx
pushq %rax #t_2__
# above is i
.loc 1 35 0
pushq $0 #t
.loc 1 36 0
.L___label_0___:
movq 8(%rsp), %rax
cmpq $0, %rax
setgeb %al
movzbq %al, %rax
testq %rax, %rax
je .L___label_1___
.loc 1 38 0
movq 24(%rsp), %rax
addq 8(%rsp), %rax
movq (%rax), %rax
movq %rax, 0(%rsp)
.loc 1 39 0
movq 24(%rsp), %rax
addq 8(%rsp), %rax
pushq %rax #t_5__
movq 24(%rsp), %rax
subq 16(%rsp), %rax
movq %rax, %rbx
movq 32(%rsp), %rax
addq %rbx, %rax
pushq %rax #t_7__
movq 8(%rsp), %rbx
popq %rax
movq (%rax), %rax
movb %al, (%rbx)
.loc 1 40 0
movq 24(%rsp), %rax
subq 16(%rsp), %rax
movq %rax, %rbx
movq 32(%rsp), %rax
addq %rbx, %rax
movq %rax, %rbx
movq 8(%rsp), %rax
movb %al, (%rbx)
.loc 1 41 0
movq 16(%rsp), %rax
subq $1, %rax
movq %rax, 16(%rsp)
addq $8, %rsp
jmp .L___label_0___
.L___label_1___:
addq $24, %rsp
xorq %rax, %rax
addq $8, %rsp
ret

printnumber:
pushq %rdi
.loc 1 47 0
pushq $.L_lit_0 #string
.loc 1 48 0
movq 8(%rsp), %rax
cmpq $0, %rax
setneb %al
movzbq %al, %rax
testq %rax, %rax
je .L___label_2___
.loc 1 50 0
pushq 8(%rsp) #y
.loc 1 51 0
movq 16(%rsp), %rax
cmpq $0, %rax
setlb %al
movzbq %al, %rax
testq %rax, %rax
je .L___label_4___
.loc 1 52 0
movq 16(%rsp), %rax
negq %rax
movq %rax, 0(%rsp)
jmp .L___label_5___
.L___label_4___:
.L___label_5___:
.loc 1 53 0
pushq $0 #c
.loc 1 54 0
.L___label_6___:
movq 8(%rsp), %rax
testq %rax, %rax
je .L___label_7___
.loc 1 56 0
movq 16(%rsp), %rax
addq 0(%rsp), %rax
pushq %rax #t_13__
movq 16(%rsp), %rax
xorq %rdx, %rdx
movq $10, %rbx
idivq %rbx
movq %rdx, %rax
movq %rax, %rbx
movq $48, %rax
addq %rbx, %rax
pushq %rax #t_15__
movq 8(%rsp), %rbx
popq %rax
movb %al, (%rbx)
.loc 1 57 0
movq 16(%rsp), %rax
xorq %rdx, %rdx
movq $10, %rbx
idivq %rbx
movq %rax, 16(%rsp)
.loc 1 58 0
movq 8(%rsp), %rax
addq $1, %rax
movq %rax, 8(%rsp)
addq $8, %rsp
jmp .L___label_6___
.L___label_7___:
.loc 1 60 0
movq 24(%rsp), %rax
cmpq $0, %rax
setlb %al
movzbq %al, %rax
testq %rax, %rax
je .L___label_8___
.loc 1 62 0
movq 16(%rsp), %rax
addq 0(%rsp), %rax
movq %rax, %rbx
movq $45, %rax
movb %al, (%rbx)
.loc 1 63 0
movq 0(%rsp), %rax
addq $1, %rax
movq %rax, 0(%rsp)
jmp .L___label_9___
.L___label_8___:
.L___label_9___:
.loc 1 65 0
movq 16(%rsp), %rax
addq 0(%rsp), %rax
movq %rax, %rbx
movq $0, %rax
movb %al, (%rbx)
.loc 1 66 0
movq 16(%rsp), %rdi
call reverse
.loc 1 67 0
movq 16(%rsp), %rax
addq 0(%rsp), %rax
movq %rax, %rbx
movq $10, %rax
movb %al, (%rbx)
.loc 1 68 0
movq 0(%rsp), %rax
addq $1, %rax
movq %rax, %rbx
movq 16(%rsp), %rax
addq %rbx, %rax
movq %rax, %rbx
movq $0, %rax
movb %al, (%rbx)
addq $16, %rsp
jmp .L___label_3___
.L___label_2___:
.loc 1 72 0
movq 0(%rsp), %rax
addq $0, %rax
movq %rax, %rbx
movq $48, %rax
movb %al, (%rbx)
.loc 1 73 0
movq 0(%rsp), %rax
addq $1, %rax
movq %rax, %rbx
movq $10, %rax
movb %al, (%rbx)
.loc 1 74 0
movq 0(%rsp), %rax
addq $2, %rax
movq %rax, %rbx
movq $0, %rax
movb %al, (%rbx)
.L___label_3___:
.loc 1 76 0
movq 0(%rsp), %rdi
call print
addq $8, %rsp
xorq %rax, %rax
addq $8, %rsp
ret


.data
.L_lit_0: .string "*********************"
.ident "Compiled with the most awesome compiler on earth!"
