#include "symbol_table.hpp"
#include <algorithm>


ValueType name_type(const std::string& n)
{
    if ( n == "int" )
        return INTEGER;
    else if ( n == "float" )
        return FLOAT;
    else if ( n == "string" )
        return STRING;
    else
        return VOID;
}


void SymbolTable::declare_var ( const label &sym )
{
    stack.push_back(sym);
}

void SymbolTable::declare_func ( const function &sym )
{
    functions.push_back(sym);
}


bool SymbolTable::is_var ( const std::string &id, bool curr_scope_only ) const
{
    std::vector<label>::const_reverse_iterator end = stack.rend();
    if ( curr_scope_only )
        end = std::find(stack.rbegin(), stack.rend(),POSITION);

    return std::find(stack.rbegin(), end, id) != end;
}

bool SymbolTable::is_func ( const std::string &id ) const
{
    return std::find(functions.rbegin(), functions.rend(), id) !=functions.rend();
}

void SymbolTable::push_scope()
{
    stack.push_back(label("__scope__",POSITION));
}
void SymbolTable::pop_scope()
{
    std::vector<label>::reverse_iterator
        scope = std::find(stack.rbegin(), stack.rend(),POSITION);
    if ( scope == stack.rend() )
        stack.clear();
    else
        stack.erase( (scope+1).base(), stack.end() );
}

const label& SymbolTable::get_var(const std::string &id ) const
{
    std::vector<label>::const_reverse_iterator
        rit = std::find(stack.rbegin(), stack.rend(), id);
    return *rit;
}
const function& SymbolTable::get_func(const std::string &id ) const
{
    std::vector<function>::const_reverse_iterator
        rit = std::find(functions.rbegin(), functions.rend(), id);
    return *rit;
}

bool SymbolTable::is_data( const std::string &id ) const
{
    for ( lit_iterator i = string_begin(); i != string_end(); ++i )
        if ( i->first == id )
            return true;
    return false;
}

label label::position(const std::string &meta)
{
    static unsigned long n = 0;
    std::ostringstream ss;
    ss << "___label_" << n++ << "___";
    return label ( ss.str(), POSITION, meta );
}
label label::temp(ValueType t)
{
    static unsigned long n = 0;
    std::ostringstream ss;
    ss << "t_" << n++ << "__";
    return label ( ss.str(), t, "", TEMP );
}
label label::string_literal()
{
    static unsigned long id = 0;
    std::ostringstream ss;
    ss << "_lit_" << id++ << "";
    return label ( ss.str(), STRING, "", LITERAL );
}
