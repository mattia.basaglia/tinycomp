#ifndef SYMBOL_TABLE_HPP_
#define SYMBOL_TABLE_HPP_
#include <vector>
#include <string>
#include <sstream>
#include <map>
//#include <boost/bimap.hpp>

enum ValueType { INTEGER, FLOAT, STRING, VOID, POSITION };
/// \return ValueType from name of a type
ValueType name_type( const std::string& n);

/*struct SymbolType
{
    ValueType type;
    bool function;

    SymbolType ( ValueType type = INTEGER, bool function = false )
        : type ( type ), function ( function ) {}

    operator ValueType& () { return type; }
    operator const ValueType& () const { return type; }
};*/

struct label
{
    std::string name;
    ValueType   type;
    std::string metadata;
    enum SpecialType { NONE, TEMP, LITERAL, FUNCTION } special;
    bool reference;

    label ( const  std::string &name = "",
           ValueType type = VOID,
           const std::string &metadata = "",
           SpecialType special = NONE )
        : name ( name ), type ( type ), metadata(metadata),
            special(special), reference(false) {}

    label ( const  char *name, ValueType type = VOID, SpecialType special = NONE )
        : name ( name ), type ( type ), special(special), reference(false) {}

    bool operator== (ValueType t) const
    {
        return type == t;
    }

    bool operator== (const std::string& n) const
    {
        return name == n;
    }

    bool operator == ( const label& l ) const
    {
        return type == l.type && name == l.name && reference == l.reference;
    }

    static label position(const std::string &meta="");
    static label temp(ValueType t);
    static label string_literal();
    static label int_literal ( const std::string &value )
    {
        return label ( value, INTEGER, "", LITERAL );
    }

};



struct function : public label
{
    std::vector<label> parameters;
    function ( ValueType ret_type, const  std::string &name = "" )
     : label ( name, ret_type, "", FUNCTION ) {}
};


class SymbolTable
{
    protected:
        /*struct literal_table
        {
            unsigned next_id;
            typedef boost::bimap<std::string,unsigned> bm;
            bm map;
            literal_table() : next_id(0) {}

            unsigned insert(const std::string& s)
            {
                std::pair<bm::iterator, bool> p = map.insert ( bm::value_type(s,next_id) );
                if ( !p.second )
                    return p.first->right;
                return next_id++;
            }
            unsigned get(const std::string& s) const
            {
                return map.left.at(s);
            }
            const std::string&get( unsigned u ) const
            {
                return map.right.at(u);
            }
        };*/

        std::vector<label> stack; // use type POSITION for placeholders
        std::vector<function> functions;
        std::map<std::string,std::string> strings;

    public:
        typedef std::map<std::string,std::string>::const_iterator lit_iterator;

        void declare_var ( const label &sym );
        void declare_func ( const function &sym );
        label declare_str ( const std::string&s )
        {
            label l = label::string_literal();
            strings[l.name] = s;
            return l;
        }
        bool is_var ( const std::string &id, bool curr_scope_only=false ) const;
        bool is_func ( const std::string &id ) const;
        /// \return of 'id' is used as static data ( string literals )
        bool is_data( const std::string &id ) const;
        void push_scope();
        void pop_scope();
        const label& get_var(const std::string &id ) const;
        const function& get_func(const std::string &id ) const;

        lit_iterator string_begin() const
        {
            return strings.begin();
        }
        lit_iterator string_end() const
        {
            return strings.end();
        }
};

#endif // SYMBOL_TABLE_HPP_
