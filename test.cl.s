.file 1 "stdlib_x86_64_gas.cl"
.file 2 "test.cl"
.text
.file 3 "stdlib.incl"
.loc 3 1 0
.file "test.cl"
.loc 2 2 0
.loc 2 5 0
.loc 2 14 0
.loc 2 45 0

# Functions

.global dummy
.type dummy, @function
.global main
.type main, @function
.global undeclared
.type undeclared, @function

dummy:
pushq %rdi
pushq %rsi
pushq %rdx
pushq %rcx
.loc 2 6 0
movq 24(%rsp), %rax
addq 16(%rsp), %rax
addq 8(%rsp), %rax
addq 0(%rsp), %rax
pushq %rax #t_29__
# above is x
addq $8, %rsp
xorq %rax, %rax
addq $32, %rsp
ret

main:
.loc 2 15 0
movq $.L_lit_1, %rdi
call print
.loc 2 16 0
movq $0, %rdi
call printnumber
.loc 2 17 0
movq $123456, %rax
negq %rax
movq %rax, %rdi
call printnumber
.loc 2 19 0
pushq $111 #wtf
.loc 2 20 0
movq 0(%rsp), %rax
movq %rsp, %rax
addq $0, %rax
pushq %rax #t_34__
# above is pointer
.loc 2 21 0
movq 0(%rsp), %rax
movq (%rax), %rax
addq $12, %rax
movq 0(%rsp), %rbx
movq %rax, (%rbx)
.loc 2 22 0
movq 8(%rsp), %rdi
call printnumber
.loc 2 24 0
pushq $.L_lit_2 #string
.loc 2 25 0
movq 0(%rsp), %rax
addq $1, %rax
movq %rax, %rbx
movq $101, %rax
movb %al, (%rbx)
.loc 2 27 0
movq 0(%rsp), %rdi
call puts
.loc 2 28 0
movq 0(%rsp), %rdi
call strlen
movq %rax, %rdi
call printnumber
.loc 2 30 0
movq $456, %rax
xorq %rdx, %rdx
movq $7, %rbx
idivq %rbx
pushq %rax #t_40__
movq $123, %rdi
popq %rsi
movq $890, %rdx
movq $123, %rcx
call dummy
.loc 2 32 0
movq $8, %rdi
call malloc
pushq %rax #t_42__
# above is p
.loc 2 33 0
movq 0(%rsp), %rbx
movq $567, %rax
movq %rax, (%rbx)
.loc 2 34 0
movq 0(%rsp), %rdi
movq (%rdi), %rdi
call printnumber
.loc 2 35 0
movq 0(%rsp), %rdi
call free
.loc 2 37 0
movq $2, %rax
imulq $50, %rax
addq $25, %rax
subq $2, %rax
pushq %rax #t_47__
movq $.L_lit_3, %rdi
popq %rsi
call undeclared
pushq %rax #t_48__
# above is ret
.loc 2 38 0
movq 0(%rsp), %rdi
call printnumber
addq $40, %rsp
xorq %rax, %rax
ret

undeclared:
pushq %rdi
pushq %rsi
.loc 2 46 0
movq 8(%rsp), %rdi
call print
.loc 2 47 0
movq 0(%rsp), %rdi
call printnumber
.loc 2 48 0
movq 0(%rsp), %rax
addq $1000, %rax
addq $16, %rsp
ret
xorq %rax, %rax
addq $16, %rsp
ret


.data
.L_lit_1: .string "123sdfsdfX\n"
.L_lit_2: .string "Hcllo World!!!"
.L_lit_3: .string "Undeclared function"
.ident "Compiled with the most awesome compiler on earth!"
