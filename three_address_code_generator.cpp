#include "three_address_code_generator.hpp"


std::string type_name(ValueType t)
{
    switch ( t )
    {
        case INTEGER:   return "int";
        case FLOAT:     return "flt";
        case STRING:    return "str";
        case VOID:      return "nil";
        default:
        case POSITION:  return"";
    }
}

static std::ostream& operator<< ( std::ostream&os, const label &l )
{
    std::string n = type_name(l.type);
    if ( n != "" )
        os << '[' << n << ']';
    os << l.name;
    if ( l.metadata.size() > 0 )
        os << " (" << l.metadata << ") ";
    return os;
}


typedef three_address_code_generator _3acg;
void _3acg::assign (
                 const label& dest,
                 const label& op1,
                 const label& op2,
                 const std::string & operand )
{
    out << dest << " = " << op1 << ' ' << operand << ' ' << op2 << '\n';
}
void _3acg::assign (
                 const label& dest,
                 const label& val,
                 const std::string & operand )
{
    out << dest << " = " << operand << val << '\n';
}
void _3acg::assign (
                 const label& dest,
                 const label& val )
{
    out << dest << " = " << val << '\n';
}

void _3acg::cast ( const label& dest,
                    const label& source )
{

    std::string castfun = type_name(source.type)+'2'+type_name(dest.type);
    out << dest << " = " << castfun << " ( " << source << " )\n";
}
void _3acg::push_parameter ( const function& func,
                                     const label& param,
                                     const label&val )
{

    out << func.name << "::" << param << " = " << val << '\n';
}

void _3acg::call ( const label& get_return, const function& func )
{
    out << get_return << " = call " << func << '\n';
}
void _3acg::data ()
{
    functions();
    out << "\nDATA\n\n";
    for ( SymbolTable::lit_iterator i = pst->string_begin();
            i != pst->string_end(); ++i )
        out << i->first << " string \"" << i->second << "\"\n";
}
void _3acg::text()
{
    out << "\nTEXT\n\n";
}
void _3acg::jump ( const label& to )
{
    out << "goto " << to << '\n';
}
void _3acg::jump_if ( const label& expr, const label& to, bool negated )
{
    out << "if ";
    if ( negated )
        out << "not ";
    out << expr << " goto " << to << '\n';
}
void _3acg::exit ( const label& t )
{
    out << "exit " << t << '\n';
}
void _3acg::exit ()
{
    out << "exit 0\n";
}
void _3acg::f_return ( const label& value )
{
    out << "return " << value << '\n';
}
void _3acg::f_return ( )
{
    out << "return\n";
}
void _3acg::place_label( const label& lab )
{
    out << lab << ":\n";
}
void _3acg::enter_function ( const function& func )
{
    out.rdbuf ( func_cache.rdbuf() );
    place_label ( func );
}
void _3acg::leave_function()
{
    f_return(); /// \todo pass func && return a default value
    out.rdbuf(out_buf);
}
void _3acg::functions()
{
    out << "\nFunctions\n\n";
    out << func_cache.str();
}
void _3acg::clear_functions()
{
    func_cache.str("");
}
void _3acg::enter_scope()
{}
void _3acg::leave_scope()
{}
void _3acg::begin_parameters()
{}
void _3acg::start()
{}
void _3acg::raw( const std::string& text)
{
    out << "Inline Assembly:\n" << text << "\nEnd Inline Assembly\n";
}
void _3acg::debug_file ( const std::string& name, unsigned )
{
    out << "File: " << name << '\n';
}
void _3acg::debug_line ( unsigned, unsigned )
{}



bool _3acg::willcast ( ValueType, ValueType )
{
    return true;
}
