#ifndef THREE_ADDRESS_CODE_GENERATOR_HPP
#define THREE_ADDRESS_CODE_GENERATOR_HPP

#include "generic_generator.hpp"


class three_address_code_generator : public generic_generator
{
    public:
        three_address_code_generator ( std::ostream& out ) : generic_generator ( out ) {}
        three_address_code_generator ( const three_address_code_generator& o )
            : generic_generator ( o ) {}
        three_address_code_generator& operator= ( const three_address_code_generator& o )
        {
            generic_generator::operator= ( o );
            return *this;
        }


        virtual void assign (
                         const label& dest,
                         const label& op1,
                         const label& op2,
                         const std::string &operand );
        virtual void assign (
                         const label& dest,
                         const label& val,
                         const std::string &operand );
        virtual void assign (
                         const label& dest,
                         const label& val );
        virtual void cast ( const label& dest,
                            const label& source );
        virtual void begin_parameters();
        virtual void push_parameter ( const function& func, const label& param, const label&val );
        virtual void call ( const label& get_return, const function& func );
        virtual void f_return ( const label& value );
        virtual void f_return ( );
        virtual void data ();
        virtual void text();
        virtual void start();
        virtual void jump ( const label& to );
        virtual void jump_if ( const label& expr, const label& to, bool negated );
        virtual void exit ( const label& value );
        virtual void exit ();
        virtual void place_label ( const label& lab );
        virtual void enter_function ( const function& func );
        virtual void leave_function();
        virtual void functions();
        virtual void clear_functions();
        virtual void enter_scope();
        virtual void leave_scope();
        virtual void raw( const std::string& text);
        virtual void debug_file ( const std::string& name, unsigned file_counter );
        virtual void debug_line ( unsigned line, unsigned file_counter );

        virtual bool willcast ( ValueType, ValueType );
};

#endif // THREE_ADDRESS_CODE_GENERATOR_HPP
