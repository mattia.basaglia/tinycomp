#include "x86_64_gas_generator.hpp"


typedef x86_64_gas_generator gasg;
void gasg::assign (
                 const label& dest,
                 const label& op1,
                 const label& op2,
                 const std::string & operand )
{
    /// \todo fpu arithmetics

    if ( op2.special == label::TEMP && stack.get(op2.name) == 0 )
    {
        /// cleanup stack from unused temporaries
        op.op ( out, "popq", "%rbx" );
        stack.pop();
        allocate_rbx(op2);
    }
    unreference(op2);

    assign_to(op1,"rax");

    if ( operand == "+" )
    {
        op.op ( out, "addq", get(op2), "%rax" );
    }
    else if ( operand == "-" )
    {
        op.op ( out, "subq", get(op2), "%rax" );
    }
    else if ( operand == "*" )
    {
        op.op ( out, "imulq", get(op2), "%rax" );
    }
    else if ( operand == "/" )
    {
        op.op ( out, "xorq", "%rdx", "%rdx" );
        op.op ( out, "movq", get(op2), "%rbx" );
        op.op ( out, "idivq", "%rbx");
    }
    else if ( operand == "%" )
    {
        op.op ( out, "xorq", "%rdx", "%rdx");
        if ( ! (rbx == op2) )
            op.op ( out, "movq", get(op2), "%rbx");
        op.op ( out, "idivq", "%rbx");
        op.op ( out, "movq", "%rdx", "%rax");
    }
    else if ( operand == "<" )
    {
        op.op ( out, "cmpq", get(op2), "%rax");
        op.op ( out, "setlb", "%al");
        op.op ( out, "movzbq", "%al", "%rax");
    }
    else if ( operand == "<=" )
    {
        op.op ( out, "cmpq", get(op2), "%rax");
        op.op ( out, "setleb", "%al");
        op.op ( out, "movzbq", "%al", "%rax");
    }
    else if ( operand == "==" )
    {
        op.op ( out, "cmpq", get(op2), "%rax");
        op.op ( out, "seteb", "%al");
        op.op ( out, "movzbq", "%al", "%rax");
    }
    else if ( operand == ">=" )
    {
        op.op ( out, "cmpq", get(op2), "%rax");
        op.op ( out, "setgeb", "%al");
        op.op ( out, "movzbq", "%al", "%rax");
    }
    else if ( operand == ">" )
    {
        op.op ( out, "cmpq", get(op2), "%rax");
        op.op ( out, "setgb", "%al");
        op.op ( out, "movzbq", "%al", "%rax");
    }
    else if ( operand == "!=" )
    {
        op.op ( out, "cmpq", get(op2), "%rax");
        op.op ( out, "setneb", "%al");
        op.op ( out, "movzbq", "%al", "%rax");
    }
    else
        op.op ( out, "# unimplemented operation", operand );

    assign_rax(dest);
}

void gasg::assign_rax( const label& dest )
{
    if ( dest.reference )
    {
        op.op ( out, "movq", get(dest), "%rbx" );

        if ( dest.type == STRING )
            op.op ( out, "movb", "%al", "(%rbx)");
        else
            op.op ( out, "movq", "%rax", "(%rbx)");
    }
    else if ( stack.exists(dest.name) )
    {
        op.op ( out, "movq", "%rax", get(dest) );
    }
    else
    {
        op.op ( out, "pushq", "%rax" );
        stack.push ( dest );
    }
}

void gasg::assign (
                 const label& dest,
                 const label& val,
                 const std::string & operand )
{
    if ( val.special == label::TEMP && stack.get(val.name) == 0 )
    {
        op.op ( out, "popq", "%rax");
        stack.pop();
        if ( val.reference )
            op.op ( out, "movq", "(%rax)", "%rax");
    }
    else
        assign_to(val,"rax");

    if ( operand == "-" )
    {
        op.op ( out, "negq", "%rax");
    }
    else if ( operand == "&" )
    {
        if ( rbx == val )
            op.op ( out, "xorq", "%rax", "%rax");// getting address of a value in a register, returning 0
        else if ( val.special == label::LITERAL )
            op.op ( out,  "movq", (val.type == INTEGER ? "$" : "$.L" )+val.name, "%rax");
        else
        {
            op.op ( out, "movq", "%rsp", "%rax");
            std::ostringstream ss;
            ss << '$' << stack.get(val.name);
            op.op ( out, "addq", ss.str(), "%rax");
        }
    }
    else
        op.op ( out, "# unimplemented unary operation "+operand );

    assign_rax(dest);
}

void gasg::assign_to ( const label& val, const std::string& regist )
{
    if ( val.special == label::TEMP && stack.get(val.name) == 0 )
    {
        op.op ( out, "popq", "%"+regist );
        stack.pop();
    }
    else
    {
        std::string source = get(val);
        if ( source != '%'+regist )
            op.op ( out, "movq", get(val), '%'+regist );
    }
    if ( val.reference )
        op.op ( out, "movq", "(%"+regist+")", '%'+regist );
}

void gasg::unreference(const label& val)
{
    if ( val.reference )
    {
        if ( rbx.name == val.name )
        {
            op.op ( out, "pushq", "%rbx");
            stack.push(val);
        }
        assign_to ( val, "rbx" );
        allocate_rbx ( val );
    }
}

void gasg::assign (
                 const label& dest,
                 const label& val
                 )
{
    if ( dest.reference )
    {
        op.op ( out, "movq", get(dest), "%rbx");
        assign_to(val,"rax");
        if ( dest.type == STRING )
            op.op ( out, "movb", "%al", "(%rbx)");
        else
            op.op ( out, "movq", "%rax", "(%rbx)");
    }
    else if ( stack.exists(dest.name) )
    {
        assign_to(val,"rax");
        op.op ( out, "movq", "%rax", get(dest) );
    }
    else if ( val.special == label::TEMP && stack.get(val.name) == 0 )
    {
        if ( val.reference )
        {
            op.op ( out, "popq", "%rax");
            op.op ( out, "pushq", "(%rax)");
        }
        stack.pop();
        stack.push ( dest );
    }
    else
    {
        unreference(val);
        op.op ( out, "pushq", get(val) );
        stack.push ( dest );
    }
}

void gasg::cast ( const label& dest,
                    const label& source )
{
    if ( source.type != FLOAT && dest.type != FLOAT )
        ;
    else
        op.op ( out, "# unimplemented cast");
    /*if ( !willcast(dest.type, source.type ) )
        return;
    if ( source.type == CHAR && ( dest.type == INTEGER || dest.type == STRING ) )
    {
        op.op ( out, "movq", get(source), "%rbx");
        op.op ( out, "xorq", "%rax,", "%rax");
        op.op ( out, "movb", "%bl,", "%al");
    }
    else if ( dest.type == CHAR && ( source.type == INTEGER || source.type == STRING ) )
    {
        op.op ( out, "movq", get(source), "%rbx");
        op.op ( out, "movb", "%bl,", "%al");
    }
    else
    {
        op.op ( out, "# unimplemented cast");
        return;
    }

    if ( stack.exists(dest.name) )
        op.op ( out, "movq", "%rax,", get(dest) );
    else
    {
        op.op ( out, "pushq", "%rax");
        stack.push ( dest );
    }*/
}
void gasg::push_parameter ( const function& func,
                                     const label& param,
                                     const label&val )
{
    fparam.push_back(val);
    /*unreference(val);
    op.op ( out, "pushq", get(val), " #", func.name, "::", param.name );
    stack.push ( param );*/
}

const std::string gasg::registers[gasg::regparam] = { "rdi", "rsi", "rdx", "rcx", "r8", "r9" };

/// \note return value is supposed to be in", "%rax
void gasg::call ( const label& get_return, const function& func )
{
    /**/for ( unsigned i = 0; i != func.parameters.size(); i++ )
    {
        if ( i < regparam )
            assign_to(fparam[i],registers[i]);
        else
        {
            unreference(fparam[i]);
            op.op ( out, "pushq", get(fparam[i]) );
            stack.push ( func.parameters[i] );
        }
    }
    fparam.clear();/**/
    op.op ( out, "call", mangle(func) );
    leave_scope();
    if ( func.type != VOID )
    {
        if ( stack.exists(get_return.name) )
            op.op ( out, "movq", "%rax", get(get_return) );
        else
        {
            stack.push ( get_return );
            op.op ( out, "pushq", "%rax");
        }
    }
}
std::string gasg::mangle(const function& f)
{
    //if ( f.name == "main" )
        return f.name;
   // else
   //     return "__cl_"+f.name; // avoid conflict with C functions
}
void gasg::data ()
{
    op.flush(out);
    functions();
    out << "\n.data\n\n";
    for ( SymbolTable::lit_iterator i = pst->string_begin();
            i != pst->string_end(); ++i )
    {
        out << ".L" << i->first << ": .string \"";
        for ( unsigned j = 0; j < i->second.size(); ++j )
            switch ( i->second[j] )
            {
                case '\n' : out << "\\n"; break;
                case '\r' : out << "\\r"; break;
                case '\'' : out << "\\'"; break;
                case '\"' : out << "\\\""; break;
                default :  out << i->second[j];
            }
        out << "\"\n";
    }
    out << ".ident \"Compiled with the most awesome compiler on earth!\"\n";
}
void gasg::text()
{
    out << "\n.text\n\n";
}
void gasg::start()
{
    out << ".global _start\n_start:\n\n";
}
void gasg::jump ( const label& to )
{
    op.op ( out, "jmp", ".L"+to.name );
}
void gasg::jump_if ( const label& expr, const label& to, bool negated )
{
    assign_to(expr,"rax");
    op.op ( out, "testq", "%rax", "%rax");
    op.op ( out, ( negated ? "je" : "jne" ), ".L"+to.name );
}
void gasg::exit ( const label& t )
{
    op.op ( out, "movq", "$60", "%rax");
    assign_to(t, "rdi");
    op.op ( out, "syscall");
}
void gasg::exit ()
{
    op.op ( out, "movq", "$60", "%rax");
    op.op ( out, "xorq", "%rdi", "%rdi");
    op.op ( out, "syscall");
}
void gasg::f_return ( const label& value )
{
    assign_to(value, "rax");
    f_return();
}
void gasg::f_return ( )
{
    op.op ( out, pop_stack_function_scope() );
    op.op ( out, "ret");
    op.flush(out);
    out << '\n';
}
void gasg::place_label( const label& lab )
{
    op.flush(out);
    out << ".L" << lab.name << ":\n";
}
void gasg::enter_function ( const function& func )
{
    if ( op.prev.mnem != ".loc" )
        op.flush(out);
    stack.push_scope();
    for ( unsigned i = regparam; i < func.parameters.size(); i++ )
        stack.push ( func.parameters[i] ); // make visible params pushed from caller
    stack.push_function_scope();
    out.rdbuf ( func_cache.rdbuf() );
    defined_functions.push_back ( func );
    op.flush(out);
    out << mangle(func) << ":\n";
    for ( unsigned i = 0; i < regparam && i < func.parameters.size(); i++ )
    {
        // easy access for register-passed params
        out << "pushq " << '%' << registers[i] << '\n';
        stack.push(func.parameters[i]);
    }
}
void gasg::leave_function()
{
    op.op ( out, "xorq", "%rax", "%rax");
    f_return(); /// \todo pass func && return a default value
    stack.pop_scope();
    out.rdbuf(out_buf);
}
void gasg::functions()
{
    out << "\n# Functions\n";
    for ( std::list<function>::iterator i = defined_functions.begin();
        i != defined_functions.end(); ++i )
        out << ".global " << mangle(*i) << '\n' << ".type " << mangle(*i) << " @function\n";
    out << '\n';
    out << func_cache.str();
}
void gasg::clear_functions()
{
    func_cache.str("");
    defined_functions.clear();
}
void gasg::enter_scope()
{
    stack.push_scope();
}
void gasg::leave_scope()
{
    op.op ( out, pop_stack_scope() );
    stack.pop_scope();
}
void gasg::begin_parameters()
{
    stack.push_scope();
}
void gasg::raw( const std::string& text)
{
    op.op ( out, "# inline assembly {\n"+text+"\n# }");
}
void gasg::debug_file ( const std::string& name, unsigned file_counter )
{
    op.flush(out);
    out << ".file ";
    if ( file_counter != 0 )
        out << file_counter << " ";
    out << '"' << name << "\"\n";
}
void gasg::debug_line ( unsigned line, unsigned file_counter )
{
    //op.flush(out);
    std::ostringstream ss;
    ss << file_counter << " " << line << " 0";
    op.op ( out, ".loc", ss.str() );
}


std::string gasg::get(const label& label) const
{
    if ( rbx == label )
        return "%rbx";

    if ( label.special == label::LITERAL )
        return (label.type == INTEGER ? "$" : "$.L" )+label.name; //  literal

    std::ostringstream ss;
    ss << stack.get(label.name) << "(%rsp)";
    return ss.str();
}
std::string gasg::pop_stack_scope() const
{
    unsigned scopesize = stack.get("");
    if ( scopesize == 0 )
        return "";
    std::ostringstream ss;
    ss << "addq $" << scopesize << ", %rsp";
    return ss.str();
}
std::string gasg::pop_stack_function_scope() const
{
    unsigned scopesize = stack.get_function_scope();
    if ( scopesize == 0 )
        return "";
    std::ostringstream ss;
    ss << "addq $" << scopesize << ", %rsp";
    return ss.str();
}
void gasg::allocate_rbx(const label& id)
{
    rbx = id;
}

bool gasg::willcast ( ValueType to, ValueType from )
{
    return to != from && ( from == FLOAT || to == FLOAT );
}

void gasg::out_proxy::instruct ( const instruction& instr, std::ostream& out )
{
    if ( instr.mnem == "popq" && prev.mnem == "pushq" )
    {
        if ( instr.op1 != prev.op1 )
            out << "movq " << prev.op1 << ", " << instr.op1 << '\n';
        prev = instruction();
    }
    else
    {
        prev.print(out);
        prev = instr;
    }
}
/// \todo class push-pop optimizer
void gasg::out_proxy::instruction::print ( std::ostream&out )
{
    if ( mnem.size() == 0)
        return;

    out << mnem;
    if ( n_op > 0 )
    {
        out << ' ' << op1;
        if ( n_op == 2 )
            out << ", " << op2;
    }
    out << '\n';
}
