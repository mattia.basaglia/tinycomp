#include "x86_64_gas_generator.hpp"


typedef x86_64_gas_generator gasg;
void gasg::assign (
                 const label& dest,
                 const label& op1,
                 const label& op2,
                 const std::string & operand )
{
    /// \todo fpu arithmetics

    if ( op2.special == label::TEMP && stack.get(op2.name) == 0 )
    {
        /// cleanup stack from unused temporaries
        out << "popq %rbx\n";
        stack.pop();
        allocate_rbx(op2);
    }
    unreference(op2);

    assign_to(op1,"rax");

    if ( operand == "+" )
    {
        out << "addq " << get(op2) << ", %rax\n";
    }
    else if ( operand == "-" )
    {
        out << "subq " << get(op2) << ", %rax\n";
    }
    else if ( operand == "*" )
    {
        out << "imulq " << get(op2) << ", %rax\n";
    }
    else if ( operand == "/" )
    {
        out << "xorq %rdx, %rdx\n";
        out << "movq " << get(op2) << ", %rbx\n";
        out << "idivq %rbx\n";
    }
    else if ( operand == "%" )
    {
        out << "xorq %rdx, %rdx\n";
        if ( ! (rbx == op2) )
            out << "movq " << get(op2) << ", %rbx\n";
        out << "idivq %rbx\n";
        out << "movq %rdx, %rax\n";
    }
    else if ( operand == "<" )
    {
        out << "cmpq " << get(op2) << ", %rax\n";
        out << "setlb %al\n";
        out << "movzbq %al, %rax\n";
    }
    else if ( operand == "<=" )
    {
        out << "cmpq " << get(op2) << ", %rax\n";
        out << "setleb %al\n";
        out << "movzbq %al, %rax\n";
    }
    else if ( operand == "==" )
    {
        out << "cmpq " << get(op2) << ", %rax\n";
        out << "seteb %al\n";
        out << "movzbq %al, %rax\n";
    }
    else if ( operand == ">=" )
    {
        out << "cmpq " << get(op2) << ", %rax\n";
        out << "setgeb %al\n";
        out << "movzbq %al, %rax\n";
    }
    else if ( operand == ">" )
    {
        out << "cmpq " << get(op2) << ", %rax\n";
        out << "setgb %al\n";
        out << "movzbq %al, %rax\n";
    }
    else if ( operand == "!=" )
    {
        out << "cmpq " << get(op2) << ", %rax\n";
        out << "setneb %al\n";
        out << "movzbq %al, %rax\n";
    }
    else
        out << "# unimplemented operation " << operand << '\n';

    assign_rax(dest);
}

void gasg::assign_rax( const label& dest )
{
    if ( dest.reference )
    {
        out << "movq " << get(dest) << ", %rbx\n";

        if ( dest.type == STRING )
            out << "movb %al, (%rbx)\n";
        else
            out << "movq %rax, (%rbx)\n";
    }
    else if ( stack.exists(dest.name) )
    {
        out << "movq %rax, " << get(dest) << '\n';
    }
    else
    {
        out << "pushq %rax #" << dest.name << "\n";
        stack.push ( dest );
    }
}

void gasg::assign (
                 const label& dest,
                 const label& val,
                 const std::string & operand )
{
    if ( val.special == label::TEMP && stack.get(val.name) == 0 )
    {
        out << "popq %rax\n";
        stack.pop();
        if ( val.reference )
            out << "movq (%rax), %rax\n";
    }
    else
        assign_to(val,"rax");

    if ( operand == "-" )
    {
        out << "negq %rax\n";
    }
    else if ( operand == "&" )
    {
        if ( rbx == val )
            out << "xorq %rax, %rax # getting address of a value in a register, returning 0\n";
        else if ( val.special == label::LITERAL )
            out <<  "movq " << (val.type == INTEGER ? "$" : "$.L" )
                << val.name << ", %rax\n";
        else
        {
            out << "movq %rsp, %rax\n";
            out << "addq $" << stack.get(val.name) << ", %rax\n";
        }
    }
    else
        out << "# unimplemented unary operation " << operand << '\n';

    assign_rax(dest);
}

void gasg::assign_to ( const label& val, const std::string& regist )
{
    if ( val.special == label::TEMP && stack.get(val.name) == 0 )
    {
        out << "popq %" << regist << "\n";
        stack.pop();
    }
    else
    {
        std::string source = get(val);
        if ( source != '%'+regist )
            out << "movq " << get(val) << ", %" << regist << '\n';
    }
    if ( val.reference )
        out << "movq (%" << regist << "), %" << regist << '\n';
}

void gasg::unreference(const label& val)
{
    if ( val.reference )
    {
        if ( rbx.name == val.name )
        {
            out << "pushq %rbx\n";
            stack.push(val);
        }
        assign_to ( val, "rbx" );
        allocate_rbx ( val );
    }
}

void gasg::assign (
                 const label& dest,
                 const label& val
                 )
{
    if ( dest.reference )
    {

        if ( dest.special == label::TEMP && stack.get(dest.name) == 0 )
        {
            stack.pop();
            out << "popq %rbx\n";
        }
        else
            out << "movq " << get(dest) << ", %rbx\n";
        assign_to(val,"rax");
        if ( dest.type == STRING )
            out << "movb %al, (%rbx)\n";
        else
            out << "movq %rax, (%rbx)\n";
    }
    else if ( stack.exists(dest.name) )
    {
        assign_to(val,"rax");
        out << "movq %rax, " << get(dest) << '\n';
    }
    else if ( val.special == label::TEMP && stack.get(val.name) == 0 )
    {
        if ( val.reference )
        {
            out << "popq %rax\n";
            out << "pushq (%rax)\n";
        }
        stack.pop();
        stack.push ( dest );
        out << "# above is " <<  dest.name << '\n';
    }
    else
    {
        unreference(val);
        out << "pushq " << get(val) << " #" << dest.name << '\n';
        stack.push ( dest );
    }
}

void gasg::cast ( const label& dest,
                    const label& source )
{
    if ( source.type != FLOAT && dest.type != FLOAT )
        ;
    else
        out << "# unimplemented cast\n";
    /*if ( !willcast(dest.type, source.type ) )
        return;
    if ( source.type == CHAR && ( dest.type == INTEGER || dest.type == STRING ) )
    {
        out << "movq " << get(source) << ", %rbx\n";
        out << "xorq %rax, %rax\n";
        out << "movb %bl, %al\n";
    }
    else if ( dest.type == CHAR && ( source.type == INTEGER || source.type == STRING ) )
    {
        out << "movq " << get(source) << ", %rbx\n";
        out << "movb %bl, %al\n";
    }
    else
    {
        out << "# unimplemented cast\n";
        return;
    }

    if ( stack.exists(dest.name) )
        out << "movq %rax, " << get(dest) << '\n';
    else
    {
        out << "pushq %rax\n";
        stack.push ( dest );
    }*/
}
void gasg::push_parameter ( const function& func,
                                     const label& param, ///< ignored
                                     const label&val )
{
    fparam.push_back(val);
    /*unreference(val);
    out << "pushq " << get(val) << " #" << func.name << "::" << param.name << '\n';
    stack.push ( param );*/
}

const std::string gasg::registers[gasg::regparam] = { "rdi", "rsi", "rdx", "rcx", "r8", "r9" };

/// \note return value is supposed to be in %rax
void gasg::call ( const label& get_return, const function& func )
{
    /**/for ( unsigned i = 0; i != func.parameters.size(); i++ )
    {
        if ( i < regparam )
            assign_to(fparam[i],registers[i]);
        else
        {
            unreference(fparam[i]);
            out << "pushq " << get(fparam[i])
                << " #" << func.name << "::" << func.parameters[i].name << '\n';;
            stack.push ( func.parameters[i] );
        }
    }
    fparam.clear();/**/
    out << "call " << mangle(func) << '\n';
    leave_scope();
    if ( func.type != VOID )
    {
        if ( stack.exists(get_return.name) )
            out << "movq %rax, " << get(get_return) << '\n';
        else
        {
            stack.push ( get_return );
            out << "pushq %rax #" << get_return.name << "\n";
        }
    }
}
std::string gasg::mangle(const function& f)
{
    //if ( f.name == "main" )
        return f.name;
   // else
   //     return "__cl_"+f.name; // avoid conflict with C functions
}
void gasg::data ()
{
    functions();
    out << "\n.data\n";
    for ( SymbolTable::lit_iterator i = pst->string_begin();
            i != pst->string_end(); ++i )
    {
        out << ".L" << i->first << ": .string \"";
        for ( unsigned j = 0; j < i->second.size(); ++j )
            switch ( i->second[j] )
            {
                case '\n' : out << "\\n"; break;
                case '\r' : out << "\\r"; break;
                case '\'' : out << "\\'"; break;
                case '\"' : out << "\\\""; break;
                default : out << i->second[j];
            }
        out << "\"\n";
    }
    out << ".ident \"Compiled with the most awesome compiler on earth!\"\n";
}
void gasg::text()
{
    out << ".text\n";
}
void gasg::start()
{
    out << ".global _start\n_start:\n";
}
void gasg::jump ( const label& to )
{
    out << "jmp .L" << to.name << '\n';
}
void gasg::jump_if ( const label& expr, const label& to, bool negated )
{
    assign_to(expr,"rax");
    out << "testq %rax, %rax\n";
    out << ( negated ? "je" : "jne" ) << " .L" << to.name << '\n';
}
void gasg::exit ( const label& t )
{
    out << "movq $60, %rax #exit\n";
    assign_to(t, "rdi");
    out << "syscall\n";
}
void gasg::exit ()
{
    out << "movq $60, %rax #exit\n";
    out << "xorq %rdi, %rdi\n";
    out << "syscall\n";
}
void gasg::f_return ( const label& value )
{
    assign_to(value, "rax");
    f_return();
}
void gasg::f_return ( )
{
    out << pop_stack_function_scope();
    out << "ret\n";
}
void gasg::place_label( const label& lab )
{
    out << ".L" << lab.name << ":\n";
}
void gasg::enter_function ( const function& func )
{
    stack.push_scope();
    for ( unsigned i = regparam; i < func.parameters.size(); i++ )
        stack.push ( func.parameters[i] ); // make visible params pushed from caller
    stack.push_function_scope();
    out.rdbuf ( func_cache.rdbuf() );
    defined_functions.push_back ( func );
    out << mangle(func) << ":\n";
    for ( unsigned i = 0; i < regparam && i < func.parameters.size(); i++ )
    {
        // easy access for register-passed params
        out << "pushq %" << registers[i] << '\n';
        stack.push(func.parameters[i]);
    }
}
void gasg::leave_function()
{
    out << "xorq %rax, %rax\n";
    f_return(); /// \todo pass func && return a default value
    out << '\n';
    stack.pop_scope();
    out.rdbuf(out_buf);
}
void gasg::functions()
{
    out << "\n# Functions\n\n";
    for ( std::list<function>::iterator i = defined_functions.begin();
        i != defined_functions.end(); ++i )
        out << ".global " << mangle(*i) << '\n' << ".type " << mangle(*i) << ", @function\n";
    out << '\n';
    out << func_cache.str();
}
void gasg::clear_functions()
{
    func_cache.str("");
    defined_functions.clear();
}
void gasg::enter_scope()
{
    stack.push_scope();
}
void gasg::leave_scope()
{
    out << pop_stack_scope();
    stack.pop_scope();
}
void gasg::begin_parameters()
{
    stack.push_scope();
}
void gasg::raw( const std::string& text)
{
    out << "# inline assembly {\n" << text << "\n# }\n";
}
void gasg::debug_file ( const std::string& name, unsigned file_counter )
{
    out << ".file ";
    if ( file_counter != 0 )
        out << file_counter << " ";
    out << '"' << name << "\"\n";
}
void gasg::debug_line ( unsigned line, unsigned file_counter )
{
    out << ".loc " << file_counter << " " << line << " 0\n";
}


std::string gasg::get(const label& label) const
{
    if ( rbx == label )
        return "%rbx";

    if ( label.special == label::LITERAL )
        return (label.type == INTEGER ? "$" : "$.L" )+label.name; //  literal

    std::ostringstream ss;
    ss << stack.get(label.name) << "(%rsp)";
    return ss.str();
}
std::string gasg::pop_stack_scope() const
{
    unsigned scopesize = stack.get("");
    if ( scopesize == 0 )
        return "";
    std::ostringstream ss;
    ss << "addq $" << scopesize << ", %rsp\n";
    return ss.str();
}
std::string gasg::pop_stack_function_scope() const
{
    unsigned scopesize = stack.get_function_scope();
    if ( scopesize == 0 )
        return "";
    std::ostringstream ss;
    ss << "addq $" << scopesize << ", %rsp\n";
    return ss.str();
}
void gasg::allocate_rbx(const label& id)
{
    rbx = id;
}

bool gasg::willcast ( ValueType to, ValueType from )
{
    return to != from && ( from == FLOAT || to == FLOAT );
}
