#ifndef x86_64_gas_generator_HPP_
#define x86_64_gas_generator_HPP_

#include "generic_generator.hpp"
#include "assembly_symbol_table.hpp"
#include <list>
/// \todo generic class assebly_generator
class x86_64_gas_generator : public generic_generator
{
    protected:
        Stack stack;
        std::list<function> defined_functions;
        std::vector<label> fparam;
        label rbx;
        std::string get(const label&) const;
        std::string pop_stack_scope() const;
        std::string pop_stack_function_scope() const;
        void allocate_rbx(const label& id);
        /// moves %rax to dest
        void assign_rax( const label& dest );
        /// assigns val to regist dereferencing val if needed
        void assign_to ( const label& val, const std::string& regist );
        /// if val is a reference, dereferences it in %rbx (so that next get(val) will work properly)
        void unreference(const label& val);
        virtual std::string mangle(const function& f);

        static const unsigned regparam = 6;
        static const std::string registers[regparam];
    public:
        /// compatibility with push_pop_optimizer
        static std::string push() {return "pushq";}
        /// compatibility with push_pop_optimizer
        static std::string pop() {return "popq";}
        /// compatibility with push_pop_optimizer
        static void mov ( std::ostream &os,  const std::string&r1,  const std::string&r2 )
        { os << "movq " << r1 << ", " << r2 << '\n'; }

        x86_64_gas_generator ( std::ostream& out )
            : generic_generator ( out ), stack(8,8,8) {}
        x86_64_gas_generator ( const x86_64_gas_generator& o )
            : generic_generator ( o ), stack(8,8,8) {}
        x86_64_gas_generator& operator= ( const x86_64_gas_generator& o )
        {
            generic_generator::operator= ( o );
            return *this;
        }


        virtual void assign (
                         const label& dest,
                         const label& op1,
                         const label& op2,
                         const std::string &operand );
        virtual void assign (
                         const label& dest,
                         const label& val,
                         const std::string &operand );
        virtual void assign (
                         const label& dest,
                         const label& val );
        virtual void cast ( const label& dest,
                            const label& source );
        virtual void begin_parameters();
        virtual void push_parameter ( const function& func, const label& param, const label&val );
        virtual void call ( const label& get_return, const function& func );
        virtual void f_return ( const label& value );
        virtual void f_return ( );
        virtual void data ();
        virtual void text();
        virtual void start();
        virtual void jump ( const label& to );
        virtual void jump_if ( const label& expr, const label& to, bool negated );
        virtual void exit ( const label& value );
        virtual void exit ();
        virtual void place_label ( const label& lab );
        virtual void enter_function ( const function& func );
        virtual void leave_function();
        virtual void functions();
        virtual void clear_functions();
        virtual void enter_scope();
        virtual void leave_scope();
        virtual void raw( const std::string& text);
        virtual void debug_file ( const std::string& name, unsigned file_counter );
        virtual void debug_line ( unsigned line, unsigned file_counter );

        virtual bool willcast ( ValueType, ValueType );
};

#endif

